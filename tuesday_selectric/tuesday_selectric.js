/**
 * @file
 * Attaches the calendar behavior to all required fields.
 */

(function ($, Drupal) {
  Drupal.behaviors.tuesday_selectric = {
    attach: function (context, settings) {
      if(!$('body').hasClass('page-admin')) {
        $('select').selectric();
      }
    }
  }
})(jQuery, Drupal);