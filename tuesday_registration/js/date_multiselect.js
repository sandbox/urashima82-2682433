/**
 * @file
 * Attaches the calendar behavior to all required fields.
 */

(function ($, Drupal) {
  Drupal.behaviors.tuesday_registration = {
    attach: function (context, settings) {
      var calendar = $('#registration-multi-datepicker').multiDatesPicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0,
        numberOfMonths: [1,2],
        onSelect: refreshSummary,
      });

      var defaultDates = $('input[name="event_dates[dates]"]').val();
      defaultDates = defaultDates.split(",");      
      calendar.multiDatesPicker('addDates', defaultDates);
      refreshSummary();

      $('#registration-date-list').on('click', '.delete-event-date', function(){
        var date = new Date($(this).attr('data-event-date'));
        calendar.multiDatesPicker('removeDates', [date]);
        refreshSummary();
        return false;
      });
    },
  };

  function refreshSummary() {
    var dates = $('#registration-multi-datepicker').multiDatesPicker('getDates');
    $('input[name="event_dates[dates]"]').val(dates);

    var dateEditOuput = '<ul class="hidden-list">';
    var deleteText = Drupal.t('Delete');

    if(dates.length) {
      for (var i = 0; i < dates.length; i++) {
        dateEditOuput += '<li>';
        dateEditOuput += new Date(dates[i]).toLocaleDateString();
        dateEditOuput += ' ';
        dateEditOuput += '<a href="#" data-event-date="' + dates[i] + '" class="delete-event-date" title="' + deleteText + '">';
        dateEditOuput += '<span>' + deleteText  + '</span>';
        dateEditOuput += '</a>';
        dateEditOuput += '</li>';
      };
    }
    else {
      dateEditOuput = '<p>' + Drupal.t('No date selected.') + '</p>';
    }

    dateEditOuput += '</ul>';
    $('#registration-date-list').html(dateEditOuput);
    dipslayItemsInColumn();
  }

  function dipslayItemsInColumn() {
    var num_cols = 3,
    container = $('#registration-date-list'),
    listItem = 'li',
    listClass = 'sub-list';
    container.each(function() {
      var items_per_col = new Array(),
      items = $(this).find(listItem),
      min_items_per_col = Math.floor(items.length / num_cols),
      difference = items.length - (min_items_per_col * num_cols);
      for (var i = 0; i < num_cols; i++) {
        if (i < difference) {
          items_per_col[i] = min_items_per_col + 1;
        } else {
          items_per_col[i] = min_items_per_col;
        }
      }
      for (var i = 0; i < num_cols; i++) {
        $(this).append($('<ul ></ul>').addClass(listClass));
        for (var j = 0; j < items_per_col[i]; j++) {
          var pointer = 0;
          for (var k = 0; k < i; k++) {
            pointer += items_per_col[k];
          }
          $(this).find('.' + listClass).last().append(items[j + pointer]);
        }
      }
    });
  }

})(jQuery, Drupal);