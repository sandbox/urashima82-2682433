<?php
/**
 * @file
 * An example field using the Field Types API.
 */

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function tuesday_registration_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'tuesday_registration_dates' => array(
      'label' => t('Tuesday registration dates'),
      'description' => t('Only used for registration type to display list of dates.'),
      'default_widget' => 'tuesday_registration_dates_widget',
      'default_formatter' => 'tuesday_registration_dates_formatter',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function tuesday_registration_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];

  $date_formats = array();
  foreach (system_get_date_types() as $type => $date_format) {
    $date_formats[$type] = $date_format['title'];
  }
  
  $form['date_format'] = array(
    '#type' => 'select',
    '#title' => t('Date format'),
    '#description' => t('Define the date format for the event date form.'),
    '#options' => $date_formats,
    '#default_value' => isset($settings['date_format']) ? $settings['date_format'] : 'short',
  );

  $form['display_user_picture'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Display user picture'),
    '#description' => t('Display the user picture in registration form.'),
    '#default_value' => isset($settings['display_user_picture']) ? $settings['display_user_picture'] : FALSE,
  );

  $image_styles = array();
  foreach (image_styles() as $image_style_key => $image_style) {
    $image_styles[$image_style_key] = $image_style['label'];
  }

  $form['user_picture_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Define an image style for the user picture'),
    '#options' => $image_styles,
    '#default_value' => isset($settings['user_picture_image_style']) ? $settings['user_picture_image_style'] : 'thumbnail',
    '#states' => array(
      'visible' => array(
        ':input[name="field[settings][display_user_picture]"]' => array('checked' => TRUE),
      ),
    ),
  );

  return $form;
}

/**
 * Implements hook_field_is_empty().
 */
function tuesday_registration_field_is_empty($item, $field) {
  return empty($item['dates']);
}

/**
 * Implements hook_field_formatter_info().
 */
function tuesday_registration_field_formatter_info() {
  return array(
    'tuesday_registration_dates_formatter' => array(
      'label' => t('Registration dates'),
      'field types' => array('tuesday_registration_dates'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function tuesday_registration_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    
    case 'tuesday_registration_dates_formatter':
      $settings = $field['settings'];
      $date_format = isset($settings['date_format']) ? $settings['date_format'] : 'short';
      foreach ($items as $delta => $item) {
        $date = new DateTime($item['dates']);
        $element[] = array(
          '#markup' => format_date($date->getTimestamp(), $date_format),
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function tuesday_registration_field_widget_info() {
  return array(
    'tuesday_registration_dates_widget' => array(
      'label' => t('Registration date form'),
      'field types' => array('tuesday_registration_dates'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function tuesday_registration_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  global $user;
  $settings = $field['settings'];
  $date_format = isset($settings['date_format']) ? $settings['date_format'] : 'short';
  $display_user_picture = isset($settings['display_user_picture']) ? $settings['display_user_picture'] : FALSE;
  $user_picture_image_style = isset($settings['user_picture_image_style']) ? $settings['user_picture_image_style'] : 'thumbnail';
  $main_widget = $element;

  switch ($instance['widget']['type']) {

    case 'tuesday_registration_dates_widget':
      $entity = (array) $element['#entity'];
      
      if(!empty($entity)) {

        $registration_settings = db_select('tuesday_registration_event_settings', 't')
          ->fields('t')
          ->condition('entity_type', $entity['entity_type'],'=')
          ->condition('entity_id', $entity['entity_id'],'=')
          ->range(0,1)
          ->execute()
          ->fetchAssoc();

        if(!empty($registration_settings)) {
          drupal_add_css(drupal_get_path('module', 'tuesday_registration') . '/css/tuesday_registration_form.css');
          $values = isset($items[$delta]) ? $items[$delta] : array();

          $dates = array();
          if(!empty($registration_settings['dates'])) {
            $dates = unserialize($registration_settings['dates']);
            $dates = !empty($dates[0]) ? array_combine($dates, $dates) : array();

            foreach ($dates as $key => $value) {
              $event_date = new DateTime($value, new DateTimeZone($user->timezone));

              $currentDate = new DateTime();
              $currentDate->setTimezone(new DateTimeZone($user->timezone));
              $currentDate->setTime(0,0,0);

              // Remove date if it is passed.
              if($currentDate->getTimestamp() >= $event_date->getTimestamp()) {
                unset($dates[$key]);
              }
              else {              
                $dates[$key] = format_date($event_date->getTimestamp(), $date_format);
              }
            }
          }

          if(!empty($dates)) {

            $main_widget['dates_actual_choice_label'] = array(
              '#markup' => '<div class="label-wrapper"><label>' . $instance['label'] . '</label></div>',
            );

            if(isset($values['dates'])) {

              $dates_actual_choice = new DateTime($values['dates']);
              $dates_actual_choice = format_date($dates_actual_choice->getTimestamp(), $date_format);
              $main_widget['dates_actual_choice'] = array(
                '#markup' => '<span class="current-choice-label">' . t('Current choice') . ' :</span> '.
                             '<span class="current-choice">' . $dates_actual_choice . '</span>',
                '#prefix' => '<div class="current-choice-wrapper">',
                '#suffix' => '</div>',
              );
            }

            if($display_user_picture) {
              
              $registration = (array) $element['#entity'];
              $current_user = $registration['user_uid'];
              $user_picture = '';
              $path = '';
              $default_picture = variable_get('user_picture_default', '');
              $current_user = !empty($current_user) ? user_load($current_user) : user_load($user->uid);
              
              if (!empty($current_user->picture)) {
                $path = $current_user->picture->uri;
                $user_picture = theme('image_style', array('path' => $path, 'style_name' => $user_picture_image_style));
              }
              elseif(!empty($default_picture)) {
                $realpath = variable_get('file_public_path', conf_path() . '/files');
                $default_picture = str_replace($realpath, '', $default_picture);
                $path = file_build_uri($default_picture);
                $user_picture = theme('image_style', array('path' => $path, 'style_name' => $user_picture_image_style));
              }
              else {
                $default_picture = '/' . drupal_get_path('module', 'tuesday_registration') .  '/css/img/default-avatar.png';
                $user_picture = '<img class="default-avatar" src ="' . $default_picture . '" width="140px" height="140px"/>';
              }
              
              $main_widget['user_picture'] =  array(
                '#prefix' => '<div class="user-picture-wrapper">',
                '#suffix' => '</div>',
                '#markup'=> $user_picture,
              );
            }

            $extra_classes = array();
            $extra_classes[] = 'dates-wrapper';
            $extra_classes[] = $display_user_picture ? 'has-user-picture' : 'no-user-picture';

            $main_widget['dates'] = array(
              '#title' => t('Select a date'),
              '#type' => 'radios',
              '#prefix' => '<div class="' . implode(' ', $extra_classes) . '">',
              '#suffix' => '</div>',
              '#options' => $dates,
              '#default_value' => isset($values['dates']) ? $values['dates'] : NULL,
            );
          }
        }

      }
      break;
  }

  $element['dates'] = $main_widget;
  return $element;
}

/**
 * Implements hook_field_validate().
 */
function tuesday_registration_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if(!isset($item['dates']['dates']) || empty($item['dates']['dates'])) {
       $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'dates_invalid',
        'message' => t('The field <em>@field_label</em> is required. Please choose a date.', array('@field_label' => $instance['label'])),
      );
    }
  }
}

/**
 * Implements hook_field_widget_error().
 */
function tuesday_registration_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'dates_invalid':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Implements hook_form_field_ui_field_edit_form_alter().
 */
function tuesday_registration_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#field']['type'] == 'tuesday_registration_dates') {
    $form['field']['cardinality']['#options'] = array(1 => 1);
    $form['field']['cardinality']['#access'] = FALSE;
  }
}