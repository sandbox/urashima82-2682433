<?php

/**
 * Callback function from hook_menu().
 */
function tuesday_popup_admin($form, &$form_state) {
  $form = array();

  $form['action']['links'] = array(
    '#type' => 'markup',
    '#markup' => '',
    '#prefix' => '<ul class="action-links">',
    '#suffix' => '</ul>',

    'add_popup' => array(
      '#type' => 'markup',
      '#markup' => l(t('Add a popup'),'/admin/config/tuesday/tuesday-popup/add'),
      '#prefix' => '<li>',
      '#suffix' => '</li>',
    )
  );

  $header = array(
    array('data' => t('#Id'), 'field' => 'pp.tuesday_popup_id'),
    array('data' => t('Name'), 'field' => 'pp.name'),
    array('data' => t('Is active'), 'field' => 'pp.is_active'),    
    array('data' => t('Action')),
  );

  $result = db_select('tuesday_popup', 'pp')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(50)
    ->fields('pp')
    ->orderByHeader($header)
    ->execute();

  $rows = array();

  if($result->rowCount()) {
    foreach ($result as $row) {
      $final_row = (array) $row;

      $final_row['is_active'] = $final_row['is_active'] ? t('Yes') : t('No');
      unset($final_row['content']);
      unset($final_row['format']);
      unset($final_row['extra_config']);

      $final_row['action'] = ' <a href="/admin/config/tuesday/tuesday-popup/edit/' . $final_row['tuesday_popup_id'] . '">' . t('Edit') . '</a> ';
      $final_row['action'] .= ' <a href="/admin/config/tuesday/tuesday-popup/delete/' . $final_row['tuesday_popup_id'] . '">' . t('Delete') . '</a> ';

      $rows[] = array('data' => $final_row);
    }
  }
  else {
    $rows[] = array(
      'default_button' => array(
        'data' => '<a href="/admin/config/tuesday/tuesday-popup/add" class="button">' . t('Add a Popup') . '</a>',
        'colspan' => 4,
      ),
    );
  }

  // Build the table for the nice output.
  $build['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  $build['tablesort_pager'] = array(
    '#theme' => 'pager',
  );

  $form['tablesort_table'] = array(
    '#type' => 'markup',
    'markup' => $build['tablesort_table'],
  );

  $form['tablesort_pager'] = array('#markup' => theme('pager'));

  return $form;
}