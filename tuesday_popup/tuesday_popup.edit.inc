<?php

/**
 * Callback function for editing popups.
 */
function tuesday_popup_edit($form, &$form_state, $tuesday_popup_id = 0) {
  $form = array();

  $form['name'] = array(
    '#title' => t('Popup name'),
    '#description' => t('Define a name for the popup. <em>Only used for administration</em>.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );

  $form['is_active'] = array(
    '#title' => t('Is active'),
    '#description' => t('Set the status of the popup. If is not check, the popup will never be displayed.'),
    '#type' => 'checkbox',
    '#default_value' => TRUE,
  );

  $form['content'] = array(
    '#title' => t('Content'),
    '#type' => 'text_format',
    '#format' => NULL,
  );

  $form['extra_config'] = array(
    '#title' => t('Configuration'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  $form['extra_config']['modal_type'] = array(
    '#title' => t('Type of modal'),
    '#description' => t('Define the type of the popup modal to use.'),
    '#type' => 'select',
    '#required' => TRUE,
    '#empty_option' => t('- Select an option -'),
    '#options' => array(
      'bootstrap' => t('Bootstrap modal'),
      'colorbox' => t('Colorbox modal'),
      'expand_banner' => t('Expand Banner'),
    ),
  );

  $default_theme = variable_get('theme_default');
  $popup_regions = system_region_list($default_theme);
  $excluded_regions = array(
    'dashboard_main',
    'dashboard_sidebar',
    'dashboard_inactive',
  );
  foreach ($popup_regions as $popup_region => $popup_region_label) {
    if(in_array($popup_region, $excluded_regions)) {
      unset($popup_regions[$popup_region]);
    }
  }

  $form['extra_config']['region'] = array(
    '#title' => t('Region'),
    '#description' => t('Define the region where popup has to appears.'),
    '#type' => 'select',
    '#options' => $popup_regions,
    '#empty_option' => t('- Select -'),
    '#states' => array(
      'visible' => array(
        ':input[name="modal_type"]' => array('value' => 'expand_banner'),
      ),
    ),
  );

  $form['extra_config']['delay'] = array(
    '#title' => t('Popup delay'),
    '#required' => TRUE,
    '#description' => t('Define the popup delay (in seconds) before being displayed.'),
    '#type' => 'textfield',
    '#element_validate' => array('element_validate_integer_positive'), 
  );

  $form['extra_config']['cookie_lifetime'] = array(
    '#title' => t('Cookie lifetime'),
    '#required' => TRUE,
    '#description' => t('Define the delay (in seconds) for a new display of the same popup.'),
    '#type' => 'textfield',
    '#element_validate' => array('element_validate_integer_positive'), 
  );

  $form['extra_config']['path'] = array(
    '#title' => t('Popup path'),
    '#description' => t('Define the path where the popup will be displayed.<br>
      <strong>Set one path per line. Leave empty to display the popup anywhere. Use <em>&lt;front&gt;</em> to display the popup in the homepage.</strong>'),
    '#type' => 'textarea',
  );

  $form['extra_config']['weight'] = array(
    '#title' => t('Weight'),
    '#description' => t('Define the weight of the popup if you have multiple popup that can be displayed in the same page to priorise.<br>
      <strong>The higher the weight, the higher the popup is displayed later.</strong>'),
    '#type' => 'weight',
    '#delta' => 10,
  );

  $form['tuesday_popup_id'] = array(
    '#type' => 'hidden',
    '#value' => $tuesday_popup_id,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['#submit'][] = 'tuesday_popup_edit_submit';

  if(!empty($tuesday_popup_id)) {
    $tuesday_popup = db_select('tuesday_popup', 'pp')
      ->fields('pp')
      ->condition('tuesday_popup_id', $tuesday_popup_id, '=')
      ->execute()
      ->fetchAssoc();

    $tuesday_popup['extra_config'] = unserialize($tuesday_popup['extra_config']);

    $form['name']['#default_value'] = $tuesday_popup['name'];
    $form['is_active']['#default_value'] = $tuesday_popup['is_active'];
    $form['content']['#default_value'] = $tuesday_popup['content'];
    $form['content']['#format'] = $tuesday_popup['format'];
    $form['extra_config']['weight']['#default_value'] = $tuesday_popup['extra_config']['weight'];
    $form['extra_config']['modal_type']['#default_value'] = $tuesday_popup['extra_config']['modal_type'];
    $form['extra_config']['region']['#default_value'] = isset($tuesday_popup['extra_config']['region']) ? $tuesday_popup['extra_config']['region'] : NULL;
    $form['extra_config']['delay']['#default_value'] = $tuesday_popup['extra_config']['delay'];
    $form['extra_config']['cookie_lifetime']['#default_value'] = $tuesday_popup['extra_config']['cookie_lifetime'];
    $form['extra_config']['path']['#default_value'] = $tuesday_popup['extra_config']['path'];
  }

  return $form;
}

function tuesday_popup_edit_submit($form, &$form_state) {
  $values =  $form_state['values'];
  $extra_config = array(
    'weight' => $values['weight'],
    'modal_type' => $values['modal_type'],
    'region' => isset($values['region']) ? $values['region'] : '',
    'delay' => $values['delay'],
    'cookie_lifetime' => $values['cookie_lifetime'],
    'path' => $values['path'],
  );

  $tuesday_popup = array(
    'name' => $values['name'],
    'is_active' => $values['is_active'],
    'content' => $values['content']['value'],
    'format' => $values['content']['format'],
    'extra_config' => $extra_config,
  );
  
  if(!empty($values['tuesday_popup_id'])) {
    $tuesday_popup['tuesday_popup_id'] = $values['tuesday_popup_id'];
    drupal_write_record('tuesday_popup', $tuesday_popup, 'tuesday_popup_id');
  }
  else {
    drupal_write_record('tuesday_popup', $tuesday_popup); 
  }

  drupal_set_message(t('Popup has been created.'), 'status');
  drupal_goto('/admin/config/tuesday/tuesday-popup');
}