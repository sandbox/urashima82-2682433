(function($, Drupal) {
  Drupal.behaviors.tuesday_popup = {
    attach: function(context, settings) {
      if(typeof settings.tuesday_popup != 'undefined') {
        var path = settings.tuesday_popup.path;
        $.getJSON("/tuesday/popup/get-setting?path=" + path, function( popupSettings ) {
          if (popupSettings != null) {

            var extraConfig = popupSettings.tuesday_popup.extra_config;
            var popupId = popupSettings.tuesday_popup.tuesday_popup_id;
            var timeServer = popupSettings.tuesday_popup.time_server;
            var delay = parseInt(extraConfig.delay) * 1000;
            var cookieName = 'Drupal.visitor.tuesday.popup.' + popupId;

            var container = '#tuesday-popup-wrapper-page-bottom';
            if(extraConfig.modal_type == 'expand_banner') {
              container = '#tuesday-popup-wrapper-page-top';
              region = extraConfig.region;
              region = region.replace(/_/g, '-');
              container = '#tuesday-popup-wrapper-' + region;
            }

            $.getJSON("/tuesday/popup/get-json/" + popupId, function( popup ) {

                $(container).html(popup.content);
                $('#tuesday-popup').hide();
                $('.close-expand-banner').click(function(){
                  $('#tuesday-popup').hide('slow');
                })

                setTimeout(function(){
                  if ($(context).length && $(context)[0].nodeName == '#document') {
                    $.cookie(cookieName, timeServer);

                    if(extraConfig.modal_type == 'bootstrap') {
                      $('#module-tuesday-popup').modal('toggle');
                    }
                    else if(extraConfig.modal_type == 'colorbox') {
                      $('#module-tuesday-popup-link', context).once('init-colorbox-inline').colorbox({
                        transition:settings.colorbox.transition,
                        speed:settings.colorbox.speed,
                        opacity:settings.colorbox.opacity,
                        slideshow:settings.colorbox.slideshow,
                        slideshowAuto:settings.colorbox.slideshowAuto,
                        slideshowSpeed:settings.colorbox.slideshowSpeed,
                        slideshowStart:settings.colorbox.slideshowStart,
                        slideshowStop:settings.colorbox.slideshowStop,
                        current:settings.colorbox.current,
                        previous:settings.colorbox.previous,
                        next:settings.colorbox.next,
                        close:settings.colorbox.close,
                        overlayClose:settings.colorbox.overlayClose,
                        maxWidth:settings.colorbox.maxWidth,
                        maxHeight:settings.colorbox.maxHeight,
                        innerWidth:function(){
                          return $.urlParam('width', $(this).attr('href'));
                        },
                        innerHeight:function(){
                          return $.urlParam('height', $(this).attr('href'));
                        },
                        title:function(){
                          return decodeURIComponent($.urlParam('title', $(this).attr('href')));
                        },
                        iframe:function(){
                          return $.urlParam('iframe', $(this).attr('href'));
                        },
                        inline:function(){
                          return $.urlParam('inline', $(this).attr('href'));
                        },
                        href:function(){
                          return $.urlParam('fragment', $(this).attr('href'));
                        }
                      });
                      $('#module-tuesday-popup-link').click();
                    }
                    else if(extraConfig.modal_type == 'expand_banner') {
                      $('#tuesday-popup').show('slow');
                    }
                  }
                }, delay);
            });
          }

        });

      }
    }
  }
})(jQuery, Drupal);
