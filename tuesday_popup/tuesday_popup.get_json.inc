<?php


/**
 * Callback function for editing popups.
 */
function tuesday_popup_get_json($tuesday_popup_id = 0) {
  $results = db_select('tuesday_popup', 'pp')
    ->fields('pp')
    ->condition('is_active', TRUE,'=')
    ->condition('tuesday_popup_id', $tuesday_popup_id,'=')
    ->execute()
    ->fetchAll();

  if(!empty($results)) {
    $popup = $results[0];
    $popup->extra_config = unserialize($popup->extra_config);
    $popup->content =  theme('tuesday_popup_template_' . $popup->extra_config['modal_type'], array('content' => $popup->content));
    drupal_json_output($popup);
    exit;
  }
}