<?php

/**
 * Callback function from hook_menu().
 */
function tuesday_popup_delete($form, &$form_state, $tuesday_popup_id = 0) {
  
  $tuesday_popup_delete = db_select('tuesday_popup', 'pp')
    ->fields('pp')
    ->condition('tuesday_popup_id', $tuesday_popup_id, '=')
    ->execute()
    ->fetchAssoc();

  $form = array();

  $form['question'] = array(
    '#type' => 'markup',
    '#markup' => t(
      '<p>Are you sure you want to delete this Popup: <em>@tuesday_popup_name</em> ? </p>', 
      array('@tuesday_popup_name' => $tuesday_popup_delete['name'])
    ),
  );

  $form['tuesday_popup_id'] = array(
    '#type' => 'hidden',
    '#value' => $tuesday_popup_id,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  $form['actions']['#submit'][] = 'tuesday_popup_delete_submit';
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), '/admin/config/tuesday/tuesday-popup'),
  );

  return $form;
}

function tuesday_popup_delete_submit($form, &$form_state) {
  $tuesday_popup_id =  $form_state['values']['tuesday_popup_id'];

  db_delete('tuesday_popup')
    ->condition('tuesday_popup_id', $tuesday_popup_id)
    ->execute();
  
  drupal_set_message(t('Popup has been deleted.'), 'status');
  drupal_goto('/admin/config/tuesday/tuesday-popup');
}