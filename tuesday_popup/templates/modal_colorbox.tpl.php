<div id="module-tuesday-popup-colorbox-wrapper" style="display: none;">
  <div id="module-tuesday-popup">
    <?php print $content; ?>
  </div>
  <a id="module-tuesday-popup-link" class="colorbox-inline" href="?width=500&height=500&inline=true#module-tuesday-popup"></a>
</div>