<div id="tuesday-popup">
  <div class="container">
    <div class="expand-banner">
      <button type="button" class="close-expand-banner">×</button>
      <div class="banner-body">
        <?php print $content; ?>
      </div>
    </div>
  </div>
</div>