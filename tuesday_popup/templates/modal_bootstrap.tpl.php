<div id="module-tuesday-popup" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <?php print $content; ?>
      </div>
      <!-- <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><?php print t('Close'); ?></button>
      </div> -->
    </div>
  </div>
</div>
