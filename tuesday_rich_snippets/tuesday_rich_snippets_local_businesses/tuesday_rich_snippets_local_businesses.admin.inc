<?php

/**
 * Callback function from hook_menu().
 * Item : admin/config/tuesday/rich-snippets/local-businesses
 */
function tuesday_rich_snippets_local_businesses_admin($form, &$form_state) {
  $form = array();

  $form['action']['add_local_business'] = array(
    '#type' => 'markup',
    '#markup' => '',
    '#prefix' => '<ul class="action-links">',
    '#suffix' => '</ul>',

    'add_local_business' => array(
      '#type' => 'markup',
      '#markup' => l(t('Add a Local Business'),'/admin/config/tuesday/rich-snippets/local-businesses/add'),
      '#prefix' => '<li>',
      '#suffix' => '</li>',
    )
  );

  $header = array(
    array('data' => t('#Id'), 'field' => 'lb.local_business_id'),
    array('data' => t('Name'), 'field' => 'lb.name'),
    array('data' => t('Target path'), 'field' => 'lb.target_path'),
    array('data' => t('Is department'), 'field' => 'lb.is_department'),
    array('data' => t('Parent Local Business'), 'field' => 'lb.parent_local_business'),    
    array('data' => t('Local Business Data')),
    array('data' => t('Action')),
  );

  $result = db_select('tuesday_rich_snippets_local_businesses', 'lb')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(50)
    ->fields('lb')
    ->orderByHeader($header)
    ->execute();

  $rows = array();

  if($result->rowCount()) {
    foreach ($result as $row) {
      $final_row = (array) $row;
      $final_row['action'] = ' <a href="/admin/config/tuesday/rich-snippets/local-businesses/edit/' . $final_row['local_business_id'] . '">' . t('Edit') . '</a> ';
      $final_row['action'] .= ' <a href="/admin/config/tuesday/rich-snippets/local-businesses/delete/' . $final_row['local_business_id'] . '">' . t('Delete') . '</a> ';

      $local_business_data = unserialize($final_row['local_business_data']);
      $output = '<strong>Type:</strong> ' . $local_business_data['type'];
      if(isset($local_business_data['telephone'])){
        $output .= ' <strong>Telephone:</strong> ' . $local_business_data['telephone'];
      }
      
      $final_row['is_department'] = $final_row['is_department'] ? t('Yes') : t('No');

      $parent_local_business = TuesdayRichSnippetsLocalBusinessApi::getLocalBusinessById($final_row['parent_local_business_id']);
      $final_row['parent_local_business_id'] = isset($parent_local_business['name']) ? $parent_local_business['name'] : '';

      $final_row['target_path'] = ($final_row['target_path'] ==  '<front>') ? t('Home') : $final_row['target_path'];
      if(drupal_get_path_alias($final_row['target_path']) != $final_row['target_path']) {
        $final_row['target_path'] .= '<br>Alias: <em>' . drupal_get_path_alias($final_row['target_path']) . '</em>';
      }

      $final_row['local_business_data'] = $output;

      $rows[] = array('data' => $final_row);
    }
  }
  else {
    $rows[] = array(
      'default_button' => array(
        'data' => '<a href="/admin/config/tuesday/rich-snippets/local-businesses/add" class="button">' . t('Add a Local Business') . '</a>',
        'colspan' => 7,
      ),
    );
  }

  // Build the table for the nice output.
  $build['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  $build['tablesort_pager'] = array(
    '#theme' => 'pager',
  );

  $form['tablesort_table'] = array(
    '#type' => 'markup',
    'markup' => $build['tablesort_table'],
  );

  $form['tablesort_pager'] = array('#markup' => theme('pager'));

  return $form;
}