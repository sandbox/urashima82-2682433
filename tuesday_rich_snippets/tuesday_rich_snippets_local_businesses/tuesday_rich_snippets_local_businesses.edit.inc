<?php
/**
 * Callback function from hook_menu().
 * Item : admin/config/tuesday/rich-snippets/local-businesses/add
 * Item : admin/config/tuesday/rich-snippets/local-businesses/edit/%
 */
function tuesday_rich_snippets_local_businesses_edit($form, &$form_state, $local_business_id = 0) {

  $local_business_edit_data_count = 0;
  if(!empty($local_business_id)){
    $local_business_edit = TuesdayRichSnippetsLocalBusinessApi::getLocalBusinessById($local_business_id);
    $local_business_edit_data_count = count($local_business_edit['local_business_data']['opening_hour_specification']);
  }

  // Managing Ajax calls from form's elements
  $opening_hour_specification_items = isset($form_state['storage']['opening_hour_specification_items']) ? $form_state['storage']['opening_hour_specification_items'] : 0;

  $opening_hour_specification_items += $local_business_edit_data_count;

  $form_state['storage']['opening_hour_specification_items_to_delele'] = -1;

  // If delete an Opening Hour Specification.
  if(isset($form_state['triggering_element']['#name']) && $form_state['triggering_element']['#name'] != 'add-opening-hour') {
    $element_to_delete = $form_state['triggering_element']['#name'];
    $index_to_delete = str_replace('delete-opening-hour-', '', $element_to_delete);

    $form_state['storage']['opening_hour_specification_items_to_delele'] = $index_to_delete;
  }

  // If add an Opening Hour Specification.
  if(isset($form_state['triggering_element']['#name']) && $form_state['triggering_element']['#name'] == 'add-opening-hour') {
    $opening_hour_specification_items++;
  }

  // If get coordinates.
  if(isset($form_state['triggering_element']['#name']) && $form_state['triggering_element']['#name'] == 'get-coordinate') {
    $get_coordinates = TRUE;
  }

  $form = array();

  // Add css to form.
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'tuesday_rich_snippets_local_businesses') . '/css/tuesday_rich_snippets_local_businesses.css',
  );

  $form['local_business_id'] = array(
    '#type' => 'hidden',
    '#value' => $local_business_id,
  );

  if (!empty($local_business_edit)) {
    $is_department = $local_business_edit['is_department'];
    $parent_local_business_id = isset($local_business_edit['parent_local_business_id']) ? $local_business_edit['parent_local_business_id'] : NULL; 
    $type_option_first = $local_business_edit['local_business_data']['type'];
    $subtype_option_first = isset($local_business_edit['local_business_data']['subtype']) ? $local_business_edit['local_business_data']['subtype'] : NULL;
  }
  else {
    $is_department = 0;
    $parent_local_business_id = '';
    $type_option_first = tuesday_rich_snippets_local_businesses_get_type_options();
    $type_option_first = reset($type_option_first);
    $subtype_option_first = '';
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Set the name of the Local Business.'),
    '#required' => TRUE,
    '#weight' => 0,
  );

  $form['tabs-container'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Général'),
    '#collapsible' => TRUE,
    '#group' => 'tabs-container',
    '#weight' => 1,
  );

  $form['general']['target_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Target path'),
    '#description' => t('Set path where Local Business will be displayed. (Ex: "node/123" or "&lt;front&gt;").'),
    '#required' => TRUE,
    '#weight' => 1,
  );

  $form['general']['is_department'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is this Local Business is a department ?'),
    '#weight' => 2,
    '#ajax' => array(
      'callback' => 'tuesday_rich_snippets_local_businesses_edit_ajax_get_parent_local_business_id_options',
      'wrapper' => 'parent-local-business',
    ),
  );

  $is_department = isset($form_state['values']['is_department']) ? $form_state['values']['is_department'] : $is_department;

  $form['general']['parent_local_business_id'] = array(
    '#type' => 'select',
    '#title' => t('Parent Local Business'),
    '#description' => t('Select a parent Local Business for this department.'),
    '#empty_option' => t('- Select -'),
    '#weight' => 3,
    '#required' => TRUE,
    '#options' => tuesday_rich_snippets_local_businesses_get_parents_local_business($local_business_id),
    '#default_value' => $parent_local_business_id,
    '#prefix' => '<div id="parent-local-business">',
    '#suffix' => '</div>',
  );

  if(!$is_department) {
    $form['general']['parent_local_business_id']['#type'] = 'hidden';
    unset($form['general']['parent_local_business_id']['#options']);
    unset($form['general']['parent_local_business_id']['#default_value']);
    unset($form['general']['parent_local_business_id']['#empty_option']);
    unset($form['general']['parent_local_business_id']['#required']);
  }

  $selected = isset($form_state['values']['type']) ? $form_state['values']['type'] : $type_option_first;

  $form['general']['type'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#title' => t('Type of businness'),
    '#description' => t('This list is based on Schema.org.'),
    '#options' => tuesday_rich_snippets_local_businesses_get_type_options(),
    '#empty_option' => t('- Select -'),
    '#weight' => 10,
    '#ajax' => array(
      'callback' => 'tuesday_rich_snippets_local_businesses_edit_ajax_get_type_options',
      'wrapper' => 'local-businesses-subtype',
    ),
  );

  $subtype_options = tuesday_rich_snippets_local_businesses_get_subtype_options($selected);

  $form['general']['subtype'] = array(
    '#type' => 'select',
    '#title' => t('Subtype of businness'),
    '#description' => t('This list is based on Schema.org.'),
    '#options' => $subtype_options,
    '#weight' => 11,
    '#empty_option' => t('- Select -'),
    '#default_value' => $subtype_option_first,
    '#prefix' => '<div id="local-businesses-subtype">',
    '#suffix' => '</div>',
  );

  if(!is_array($subtype_options)) {
    unset($form['general']['subtype']['#options']);
    unset($form['general']['subtype']['#default_value']);
    unset($form['general']['subtype']['#empty_option']);
    $form['general']['subtype']['#type'] = 'hidden';
  }

  $form['general']['telephone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#description' => t('Set the Local Business phone number (field based on Schema.org).'),
    '#weight' => 14,
  );

  $form['general']['faxNumber'] = array(
    '#type' => 'textfield',
    '#title' => t('Fax number'),
    '#description' => t('Set the Local Business fax number (field based on Schema.org).'),
    '#weight' => 15,
  );

  $form['address'] = array(
    '#type' => 'fieldset',
    '#title' => t('Address'),
    '#collapsible' => TRUE,
    '#group' => 'tabs-container',
    '#weight' => 2,

    'streetAddress' => array(
      '#type' => 'textfield',
      '#title' => t('Street address'),
    ),

    'postalCode' => array(
      '#type' => 'textfield',
      '#title' => t('Postal code'),
      '#size' => 10,
    ),

    'addressLocality' => array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#size' => 46,
    ),

    'addressCountry' => array(
      '#type' => 'select',
      '#title' => t('Country'),
      '#options' => TuesdayRichSnippetsLocalBusinessApi::getCountries(),
      '#empty_option' => t('- Select -'),
    ),
  );

  $form['geo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Geographic coordinates'),
    '#collapsible' => TRUE,
    '#group' => 'tabs-container',
    '#weight' => 3,

    'coordinates' => array(
      '#type' => 'container',
      '#prefix' => '<div id="coordinates">',
      '#suffix' => '</div>',


      'latitude' => array(
        '#type' => 'textfield',
        '#title' => t('Latitude'),
        '#weight' => 0,
      ),

      'longitude' => array(
        '#type' => 'textfield',
        '#title' => t('Longitude'),
        '#weight' => 1,
      ),
    ),

    'get_coordinates' => array(
      '#type' => 'button',
      '#value' => t('Get coordinates'),
      '#href' => '',
      '#name' => 'get-coordinate',
      '#ajax' => array(
        'callback' => 'tuesday_rich_snippets_local_businesses_edit_ajax_get_coordinates',
        'wrapper' => 'coordinates',
      ),
    )
  );

  if(!empty($local_business_edit)) {
    $form['name']['#default_value'] = $local_business_edit['name'];
    $form['general']['target_path']['#default_value'] = $local_business_edit['target_path'];
    $form['general']['is_department']['#default_value'] = $local_business_edit['is_department'];

    $form['general']['type']['#default_value'] = $local_business_edit['local_business_data']['type'];
    if(isset($local_business_edit['local_business_data']['telephone'])) {
      $form['general']['telephone']['#default_value'] = $local_business_edit['local_business_data']['telephone'];
    }
    if(isset($local_business_edit['local_business_data']['faxNumber'])) {
      $form['general']['faxNumber']['#default_value'] = $local_business_edit['local_business_data']['faxNumber'];
    }

    $form['geo']['coordinates']['latitude']['#default_value'] = $local_business_edit['local_business_data']['geo']['latitude'];
    $form['geo']['coordinates']['longitude']['#default_value'] = $local_business_edit['local_business_data']['geo']['longitude'];

    $form['address']['streetAddress']['#default_value'] = $local_business_edit['local_business_data']['address']['streetAddress'];
    $form['address']['postalCode']['#default_value'] = $local_business_edit['local_business_data']['address']['postalCode'];
    $form['address']['addressLocality']['#default_value'] = $local_business_edit['local_business_data']['address']['addressLocality'];
    $form['address']['addressCountry']['#default_value'] = $local_business_edit['local_business_data']['address']['addressCountry'];
  }

  if(isset($get_coordinates)) {
    $address = $form_state['values'];
    $local_business_temp = array(
      'local_business_data' => array(
        'address' => array(
          'streetAddress' => $address['streetAddress'],
          'postalCode' => $address['postalCode'],
          'addressLocality' => $address['addressLocality'],
          'addressCountry' => $address['addressCountry'],
        ),
      ),
    );

    $local_business_temp = new TuesdayRichSnippetsLocalBusinessApi($local_business_temp);
    $coordinates = $local_business_temp->getCoordinates();

    $form['geo']['coordinates']['latitude']['#value'] = $coordinates['latitude'];
    $form['geo']['coordinates']['longitude']['#value'] = $coordinates['longitude'];
  }

  $form['opening_hour_specification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Opening Hour Specification'),
    '#collapsible' => TRUE,
    '#group' => 'tabs-container',
    '#weight' => 5,
  );

  $form['opening_hour_specification']['opening_hour_specification_items'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<div id="opening-hour-specification-item">',
    '#suffix' => '</div>',
    '#weight' => 20,
  );

  // Items manage by Ajax function call.
  if ($opening_hour_specification_items) {
    // The "$count_loop" var is used to prevent 'bad' delete call and keep the right
    // numbers of items.
    $count_loop = 0;

    $days = array(
      'Monday' => t('Monday'),
      'Tuesday' => t('Tuesday'),
      'Wednesday' => t('Wednesday'),
      'Thursday' => t('Thursday'),
      'Friday' => t('Friday'),
      'Saturday' => t('Saturday'),
      'Sunday' => t('Sunday'),
    );

    for ($i = 0; $i < $opening_hour_specification_items; $i++) {
      if ($i !=  $form_state['storage']['opening_hour_specification_items_to_delele']) {

        $form['opening_hour_specification']['opening_hour_specification_items'][$i] = array(
          '#type' => 'fieldset',
          '#title' => t('Opening Hour Specification'),
        );

        $form['opening_hour_specification']['opening_hour_specification_items'][$i]['days'] = array(
          '#title' => t('Days'),
          '#type' => 'checkboxes',
          '#options' => $days,
        );

        $form['opening_hour_specification']['opening_hour_specification_items'][$i]['open'] = array(
          '#title' => t('Open'),
          '#description' => t('hh:mm'),
          '#type' => 'textfield',
          '#size' => 5,
        );

        $form['opening_hour_specification']['opening_hour_specification_items'][$i]['close'] = array(
          '#title' => t('Close'),
          '#description' => t('hh:mm'),
          '#type' => 'textfield',
          '#size' => 5,
        );

        if($i <= $local_business_edit_data_count){
          $form['opening_hour_specification']['opening_hour_specification_items'][$i]['open']['#default_value'] = $local_business_edit['local_business_data']['opening_hour_specification'][$i]['open'];
          $form['opening_hour_specification']['opening_hour_specification_items'][$i]['close']['#default_value'] = $local_business_edit['local_business_data']['opening_hour_specification'][$i]['close'];
          $form['opening_hour_specification']['opening_hour_specification_items'][$i]['days']['#default_value'] = $local_business_edit['local_business_data']['opening_hour_specification'][$i]['days'];
        }
 
        // Delete an Opening Hour Specification.
        $form['opening_hour_specification']['opening_hour_specification_items'][$i]['delete-opening-hour-' . $i] = array(
          '#type' => 'button',
          '#value' => t('Delete'),
          '#href' => '',
          '#name' => 'delete-opening-hour-' . $count_loop,
          '#ajax' => array(
            'callback' => 'tuesday_rich_snippets_local_businesses_edit_ajax_delete_opening_hours_specification',
            'wrapper' => 'opening-hour-specification-item',
          ),
        );

        $count_loop++;
      } 
    }
  }

  // Add an Opening Hour Specification.
  $form['opening_hour_specification']['add_opening_hour_specification'] = array(
    '#type' => 'button',
    '#value' => t('Add an Opening Hour Specification'),
    '#href' => '',
    '#name' => 'add-opening-hour',
    '#ajax' => array(
      'callback' => 'tuesday_rich_snippets_local_businesses_edit_ajax_add_opening_hours_specification',
      'wrapper' => 'opening-hour-specification-item',
    ),
    '#weight' => 21,
  );

  $form_state['storage']['opening_hour_specification_items'] = isset($count_loop) ? $count_loop - $local_business_edit_data_count : 0;

  $form['#validate'][] = 'tuesday_rich_snippets_local_businesses_edit_validate';
  $form['#submit'][] = 'tuesday_rich_snippets_local_businesses_edit_submit';

  $form['before_actions'] = array(
    '#markup' => '<div class="clear"></div>',
    '#weight' => 98,
  );
 
  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
    '#weight' => 99,
  );
  
  if (!empty($local_business_id)) {
    $form['actions']['delete'] = array(
      '#type' =>'link',
      '#title' => t('Delete'),
      '#href' => '/admin/config/tuesday/rich-snippets/local-businesses/delete/' . $local_business_id,
      '#attributes' => array(
        'class' => 'button',
      ),
    );
  }

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), '/admin/config/tuesday/rich-snippets/local-businesses'),
  );

  return $form;
}

function tuesday_rich_snippets_local_businesses_edit_validate($form, &$form_state) {
  // Bypass validation when ajax is called get more form parts.
  if(isset($form_state['triggering_element']['#name'])) {
    $triggering_element_name = $form_state['triggering_element']['#name'];
    $triggering_element_name_bypassed = array(
      'add-opening-hour',
      'get-coordinate',
    );

    if (in_array($triggering_element_name, $triggering_element_name_bypassed)) {
      drupal_get_messages('error');
      form_clear_error();
    }
    elseif (strpos($triggering_element_name, 'delete-opening-hour') !== FALSE) {
      drupal_get_messages('error');
      form_clear_error();
    }
  }

  $target_path = $form_state['values']['target_path'];

  $other_local_business = drupal_lookup_path('source', $form_state['values']['target_path']);
  $other_local_business = empty($other_local_business) ? $form_state['values']['target_path'] : $other_local_business;
  $other_local_business = TuesdayRichSnippetsLocalBusinessApi::getLocalBusinessByTargetPath($other_local_business);
  
  if(!empty($other_local_business['local_business_id']) && $other_local_business['local_business_id'] != $form_state['values']['local_business_id']) {
    $other_local_business_edit_url = '/admin/config/tuesday/rich-snippets/local-businesses/edit/' . $other_local_business['local_business_id'];
    $error_message = t(
      'A Local Business already exists for this <em>Target path</em>. <a href="@other_local_business_edit_url" target="_blank">Click here</a> to edit it.', 
      array('@other_local_business_edit_url' => $other_local_business_edit_url)
    );
    form_set_error('target_path', $error_message);
  }

  return $form;
}

function tuesday_rich_snippets_local_businesses_edit_submit($form, &$form_state) {
  $local_business_data = array();

  $simple_value_keys = array(
    'type',
    'subtype',
    'telephone',
    'faxNumber',
    'geo' => array(
      'latitude',
      'longitude',
    ),
    'address' => array(
      'streetAddress',
      'postalCode',
      'addressLocality',
      'addressCountry',
    ),
  );

  foreach ($simple_value_keys as $key => $value) {
    if (is_array($value)) {
      $local_business_data[$key] = array();
      foreach ($value as $sub_value) {
        $local_business_data[$key][$sub_value] = $form_state['values'][$sub_value];
      }
    }
    elseif (!empty($form_state['values'][$value])){
      $local_business_data[$value] = $form_state['values'][$value];
    }
  }

  $local_business_data['opening_hour_specification'] =  array();

  $index = 0;

  if(isset($form_state['values']['opening_hour_specification_items'])) {
    foreach ($form_state['values']['opening_hour_specification_items'] as $key => $opening_hour_specification_item) {
      $local_business_data['opening_hour_specification'][$index]['days'] =  array();

      foreach ($opening_hour_specification_item['days'] as $day) {
        if (!empty($day)) {
          $local_business_data['opening_hour_specification'][$index]['days'][] = $day;
        }
      }

      $local_business_data['opening_hour_specification'][$index]['open'] = $opening_hour_specification_item['open'];
      $local_business_data['opening_hour_specification'][$index]['close'] = $opening_hour_specification_item['close'];

      $index++;
    }
  }

  $record = array();

  $record['name'] = $form_state['values']['name'];
  
  $record['target_path'] = $form_state['values']['target_path'];
  $target_source = drupal_lookup_path('source', $record['target_path']);
  $record['target_path'] = !empty($target_source) ? $target_source : $record['target_path'];
  
  $record['is_department'] = $form_state['values']['is_department'];
  $record['parent_local_business_id'] = $form_state['values']['parent_local_business_id'];
  $record['local_business_data'] = $local_business_data;

  if(isset($form_state['values']['local_business_id'])){
    $record['local_business_id'] = $form_state['values']['local_business_id'];
  }

  $local_business = new TuesdayRichSnippetsLocalBusinessApi($record);
  $local_business->save();

  drupal_set_message('Local Business saved !', 'status');
  drupal_goto('admin/config/tuesday/rich-snippets/local-businesses');
}

/**
 * Ajax call from tuesday_rich_snippets_local_businesses_edit_form : Get coordinates.
 */
function tuesday_rich_snippets_local_businesses_edit_ajax_get_coordinates($form, &$form_state) {
  return $form['geo']['coordinates'];
}

/**
 * Ajax call from tuesday_rich_snippets_local_businesses_edit_form : Add parent local business.
 */
function tuesday_rich_snippets_local_businesses_edit_ajax_get_parent_local_business_id_options($form, &$form_state) {
  return $form['general']['parent_local_business_id'];
}

/**
 * Ajax call from tuesday_rich_snippets_local_businesses_edit_form : Add Opening Hour Specification.
 */
function tuesday_rich_snippets_local_businesses_edit_ajax_add_opening_hours_specification($form, &$form_state) {
  return $form['opening_hour_specification']['opening_hour_specification_items'];
}

/**
 * Ajax call from tuesday_rich_snippets_local_businesses_edit_form : Delete Opening Hour Specification.
 */
function tuesday_rich_snippets_local_businesses_edit_ajax_delete_opening_hours_specification($form, &$form_state) {
  return $form['opening_hour_specification']['opening_hour_specification_items'];
}

/**
 * Ajax call from tuesday_rich_snippets_local_businesses_edit_form : Get Subtype.
 */
function tuesday_rich_snippets_local_businesses_edit_ajax_get_type_options($form, &$form_state) {
  return $form['general']['subtype'];
}

function tuesday_rich_snippets_local_businesses_get_parents_local_business($local_business_id) {
  $parents_local_business = TuesdayRichSnippetsLocalBusinessApi::getParentsLocalBusinesses();

  $options = array();
  foreach ($parents_local_business as $local_business) {
    if($local_business_id != $local_business['local_business_id']) {
      $options[$local_business['local_business_id']] =  $local_business['name'];
    }
  }

  return $options;
}

/**
 * Return an assiocative array of Type options.
 */
function tuesday_rich_snippets_local_businesses_get_type_options() {
  $types = TuesdayRichSnippetsLocalBusinessApi::getTypes();
  $types = array_keys($types);
  return array_combine($types, $types);
}

/**
 * Return an assiocative array of Subtype options.
 */
function tuesday_rich_snippets_local_businesses_get_subtype_options($type) {
  $types = TuesdayRichSnippetsLocalBusinessApi::getTypes();
  return $types[$type];
}