<?php

class TuesdayRichSnippetsLocalBusinessApi {

  private static $dbTable = 'tuesday_rich_snippets_local_businesses';
  private static $dbPrimaryKey = 'local_business_id';
 
  private $localBusiness = array();

  public function __construct($localBusiness = array()) {
    $this->localBusiness = $localBusiness;
  }

  public function save() {
    if(!$this->localBusiness['is_department']) {
      $this->localBusiness['parent_local_business_id'] = '';
    }
    if(isset($this->localBusiness[self::$dbPrimaryKey]) && !empty($this->localBusiness[self::$dbPrimaryKey])) {
      drupal_write_record(self::$dbTable, $this->localBusiness, self::$dbPrimaryKey);
    }
    else {
      drupal_write_record(self::$dbTable, $this->localBusiness); 
    }
  }

  public function getJSON($keep_php_array = FALSE) {
    global $base_url;

    $structure = self::getStructureJSON();

    $local_business = $structure['main'];

    $local_business['name'] = $this->localBusiness['name'];

    if ($this->localBusiness['target_path'] == '<front>') {
      $this->localBusiness['target_path'] = $base_url;
    }
    else {
      $this->localBusiness['target_path'] = $base_url . '/' . drupal_get_path_alias($this->localBusiness['target_path']); 
    }

    $local_business['@id'] = $this->localBusiness['target_path'];
    $local_business['url'] = $this->localBusiness['target_path'];

    $local_business['@type'] = !empty($this->localBusiness['local_business_data']['subtype']) ? $this->localBusiness['local_business_data']['subtype'] : $this->localBusiness['local_business_data']['type'];

    if (isset($this->localBusiness['local_business_data']['telephone'])) {
      $local_business['telephone'] = $this->localBusiness['local_business_data']['telephone'];
    }
    if (isset($this->localBusiness['local_business_data']['faxNumber'])) {
      $local_business['faxNumber'] = $this->localBusiness['local_business_data']['faxNumber'];
    }

    $local_business['address']['streetAddress'] = $this->localBusiness['local_business_data']['address']['streetAddress'];
    $local_business['address']['postalCode'] = $this->localBusiness['local_business_data']['address']['postalCode'];
    $local_business['address']['addressLocality'] = $this->localBusiness['local_business_data']['address']['addressLocality'];
    $local_business['address']['addressCountry'] = $this->localBusiness['local_business_data']['address']['addressCountry'];

    $local_business['geo']['latitude'] = $this->localBusiness['local_business_data']['geo']['latitude'];
    $local_business['geo']['longitude'] = $this->localBusiness['local_business_data']['geo']['longitude'];

    foreach ($this->localBusiness['local_business_data']['opening_hour_specification'] as $opening_hours_specification) {
      $opening_hours_specification_final = $structure['opening_hours_specification'];

      $opening_hours_specification_final['dayOfWeek'] = $opening_hours_specification['days'];
      $opening_hours_specification_final['opens'] = $opening_hours_specification['open'];
      $opening_hours_specification_final['closes'] = $opening_hours_specification['close'];

      $local_business['openingHoursSpecification'][] = $opening_hours_specification_final;
    }

    $final = $local_business;

    if($this->localBusiness['is_department']) {
      unset($local_business['department']);

      $main_local_business = self::getLocalBusinessById($this->localBusiness['parent_local_business_id']);
      $main_local_business = new TuesdayRichSnippetsLocalBusinessApi($main_local_business);

      $final = $main_local_business->getJSON(TRUE);

      $final['department'] = $local_business;
    }

    if($keep_php_array) {
      return $final;
    }

    if (!defined('JSON_UNESCAPED_SLASHES')) {
      define('JSON_UNESCAPED_SLASHES', 64);
    }

    if (!defined('JSON_PRETTY_PRINT')) {
      define('JSON_PRETTY_PRINT', 128);
    }

    if (!defined('JSON_UNESCAPED_UNICODE')) {
      define('JSON_UNESCAPED_UNICODE', 256);
    }

    return stripslashes(json_encode($final, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
  }

  public static function delete($local_business_id) {
    db_delete(self::$dbTable)
    ->condition(self::$dbPrimaryKey, $local_business_id)
    ->execute();
  }

  public static function getLocalBusinessById($local_business_id) {
    $local_business = db_select(self::$dbTable, 'lb')
      ->fields('lb')
      ->condition(self::$dbPrimaryKey, $local_business_id, '=')
      ->execute()
      ->fetchAssoc();

    $local_business['local_business_data'] = unserialize($local_business['local_business_data']);

    return $local_business;
  }

  public static function getLocalBusinessByTargetPath($target_path) {
    $local_business = array();

    if (drupal_is_front_page()) {
      $target_path = '<front>';
    }

    $query = db_select(self::$dbTable, 'lb')
      ->fields('lb')
      ->condition('target_path', $target_path, '=')
      ->execute();
      
    if($query->rowCount()) {
      $local_business = $query->fetchAssoc();
      $local_business['local_business_data'] = unserialize($local_business['local_business_data']);
    }

    return $local_business;
  }

  public static function getParentsLocalBusinesses() {
    $parents_local_businesses = db_select(self::$dbTable, 'lb')
    ->fields('lb')
    ->condition('is_department', 0, '=')
    ->execute();

    $local_businesses = array();

    if($parents_local_businesses->rowCount()) {
      foreach ($parents_local_businesses as $local_business) {
        $local_business = (array) $local_business;
        $localBusiness['local_business_data'] = unserialize($local_business['local_business_data']);
        $local_businesses[] = $local_business;
      }
    }

    return $local_businesses;
  }

  public function getCoordinates() {
    $countries = self::getCountries();
    $address = $this->localBusiness['local_business_data']['address']['streetAddress'] . ', ' .
      $this->localBusiness['local_business_data']['address']['addressLocality'] . ', ' .
      $this->localBusiness['local_business_data']['address']['postalCode'] . ',' .
      $countries[$this->localBusiness['local_business_data']['address']['addressCountry']];

    $address = urlencode($address);
    $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=" . $address;
    $response = file_get_contents($url);
    $json = json_decode($response,true);
 
    $lat = $json['results'][0]['geometry']['location']['lat'];
    $lng = $json['results'][0]['geometry']['location']['lng'];
 
    return array('latitude' => $lat, 'longitude' => $lng);
  }

  private static function getStructureJSON() {
    $structure_main = array(
      '@type' => '',
      '@context' => 'http://schema.org',
      '@id' => '',
      'name' => '',
      'telephone' => '',
      'faxNumber' => '', 
      'address' => array(
        '@type' => 'PostalAddress',
        'streetAddress' => '',
        'addressLocality' => '',
        'postalCode' => '',
        'addressCountry' => '',
      ),
      'geo' => array(
        '@type' => 'GeoCoordinates',
        'latitude' => 0,
        'longitude' => 0,
      ),
      'url' => '',
      'openingHoursSpecification' =>  array(),
      'department' => array(),
    );

    $structure_opening_hours_specification = array(
      '@type' => 'OpeningHoursSpecification',
      'dayOfWeek' => array(),
      'opens' => '',
      'closes' => '',
    );

    $structure = array();

    $structure['main'] = $structure_main;
    $structure['opening_hours_specification'] = $structure_opening_hours_specification;
    return $structure;
  }

  public static function getTypes() {
    return array(
      'AnimalShelter' => 'AnimalShelter',
      'AutomotiveBusiness' => array(
        'AutoBodyShop' => 'AutoBodyShop',
        'AutoDealer' => 'AutoDealer',
        'AutoPartsStore' => 'AutoPartsStore',
        'AutoRental' => 'AutoRental',
        'AutoRepair' => 'AutoRepair',
        'AutoWash' => 'AutoWash',
        'GasStation' => 'GasStation',
        'MotorcycleDealer' => 'MotorcycleDealer',
        'MotorcycleRepair' => 'MotorcycleRepair',
      ),
      'ChildCare' => 'ChildCare',
      'DryCleaningOrLaundry' => 'DryCleaningOrLaundry',
      'EmergencyService' => array(
        'FireStation' => 'FireStation',
        'Hospital' => 'Hospital',
        'PoliceStation' => 'PoliceStation',
      ),
      'EmploymentAgency' => 'EmploymentAgency',
      'EntertainmentBusiness' => array(
        'AdultEntertainment' => 'AdultEntertainment',
        'AmusementPark' => 'AmusementPark',
        'ArtGallery' => 'ArtGallery',
        'Casino' => 'Casino',
        'ComedyClub' => 'ComedyClub',
        'MovieTheater' => 'MovieTheater',
        'NightClub' => 'NightClub',
      ),
      'FinancialService' => array(
        'AccountingService' => 'AccountingService',
        'AutomatedTeller' => 'AutomatedTeller',
        'BankOrCreditUnion' => 'BankOrCreditUnion',
        'InsuranceAgency' => 'InsuranceAgency',
      ),
      'FoodEstablishment' => array(
        'Bakery' => 'Bakery',
        'BarOrPub' => 'BarOrPub',
        'Brewery' => 'Brewery',
        'CafeOrCoffeeShop' => 'CafeOrCoffeeShop',
        'FastFoodRestaurant' => 'FastFoodRestaurant',
        'IceCreamShop' => 'IceCreamShop',
        'Restaurant' => 'Restaurant',
        'Winery' => 'Winery',
      ),
      'GovernmentOffice' => array(
        'PostOffice' => 'PostOffice',
      ),
      'HealthAndBeautyBusiness' => array(
        'BeautySalon' => 'BeautySalon',
        'DaySpa' => 'DaySpa',
        'HairSalon' => 'HairSalon',
        'HealthClub' => 'HealthClub',
        'NailSalon' => 'NailSalon',
        'TattooParlor' => 'TattooParlor',
      ),
      'HomeAndConstructionBusiness' => array(
        'Electrician' => 'Electrician',
        'GeneralContractor' => 'GeneralContractor',
        'HVACBusiness' => 'HVACBusiness',
        'HousePainter' => 'HousePainter',
        'Locksmith' => 'Locksmith',
        'MovingCompany' => 'MovingCompany',
        'Plumber' => 'Plumber',
        'RoofingContractor' => 'RoofingContractor',
      ),
      'InternetCafe' => 'InternetCafe',
      'LegalService' => array(
        'Attorney' => 'Attorney',
        'Notary' => 'Notary',
      ),
      'Library' => 'Library',
      'LodgingBusiness' => array(
        'BedAndBreakfast' => 'BedAndBreakfast',
        'Hostel' => 'Hostel',
        'Hotel' => 'Hotel',
        'Motel' => 'Motel',
      ),
      'MedicalOrganization' => array(
        'Dentist' => 'Dentist',
        'DiagnosticLab' => 'DiagnosticLab',
        'Hospital' => 'Hospital',
        'MedicalClinic' => 'MedicalClinic',
        'Optician' => 'Optician',
        'Pharmacy' => 'Pharmacy',
        'Physician' => 'Physician',
        'VeterinaryCare' => 'VeterinaryCare',
      ),
      'ProfessionalService' => 'ProfessionalService',
      'RadioStation' => 'RadioStation',
      'RealEstateAgent' => 'RealEstateAgent',
      'RecyclingCenter' => 'RecyclingCenter',
      'SelfStorage' => 'SelfStorage',
      'ShoppingCenter' => 'ShoppingCenter',
      'SportsActivityLocation' => array(
        'BowlingAlley' => 'BowlingAlley',
        'ExerciseGym' => 'ExerciseGym',
        'GolfCourse' => 'GolfCourse',
        'HealthClub' => 'HealthClub',
        'PublicSwimmingPool' => 'PublicSwimmingPool',
        'SkiResort' => 'SkiResort',
        'SportsClub' => 'SportsClub',
        'StadiumOrArena' => 'StadiumOrArena',
        'TennisComplex' => 'TennisComplex',
      ),
      'Store' => array(
        'AutoPartsStore' => 'AutoPartsStore',
        'BikeStore' => 'BikeStore',
        'BookStore' => 'BookStore',
        'ClothingStore' => 'ClothingStore',
        'ComputerStore' => 'ComputerStore',
        'ConvenienceStore' => 'ConvenienceStore',
        'DepartmentStore' => 'DepartmentStore',
        'ElectronicsStore' => 'ElectronicsStore',
        'Florist' => 'Florist',
        'FurnitureStore' => 'FurnitureStore',
        'GardenStore' => 'GardenStore',
        'GroceryStore' => 'GroceryStore',
        'HardwareStore' => 'HardwareStore',
        'HobbyShop' => 'HobbyShop',
        'HomeGoodsStore' => 'HomeGoodsStore',
        'JewelryStore' => 'JewelryStore',
        'LiquorStore' => 'LiquorStore',
        'MensClothingStore' => 'MensClothingStore',
        'MobilePhoneStore' => 'MobilePhoneStore',
        'MovieRentalStore' => 'MovieRentalStore',
        'MusicStore' => 'MusicStore',
        'OfficeEquipmentStore' => 'OfficeEquipmentStore',
        'OutletStore' => 'OutletStore',
        'PawnShop' => 'PawnShop',
        'PetStore' => 'PetStore',
        'ShoeStore' => 'ShoeStore',
        'SportingGoodsStore' => 'SportingGoodsStore',
        'TireShop' => 'TireShop',
        'ToyStore' => 'ToyStore',
        'WholesaleStore' => 'WholesaleStore',
      ),
      'TelevisionStation' => 'TelevisionStation',
      'TouristInformationCenter' => 'TouristInformationCenter',
      'TravelAgency' => 'TravelAgency',
    );
  }

  public static function getCountries() {
    $countries = array(
      'AF' => t('Afghanistan'),
      'AX' => t('Aland Islands'),
      'AL' => t('Albania'),
      'DZ' => t('Algeria'),
      'AS' => t('American Samoa'),
      'AD' => t('Andorra'),
      'AO' => t('Angola'),
      'AI' => t('Anguilla'),
      'AQ' => t('Antarctica'),
      'AG' => t('Antigua And Barbuda'),
      'AR' => t('Argentina'),
      'AM' => t('Armenia'),
      'AW' => t('Aruba'),
      'AU' => t('Australia'),
      'AT' => t('Austria'),
      'AZ' => t('Azerbaijan'),
      'BS' => t('Bahamas'),
      'BH' => t('Bahrain'),
      'BD' => t('Bangladesh'),
      'BB' => t('Barbados'),
      'BY' => t('Belarus'),
      'BE' => t('Belgium'),
      'BZ' => t('Belize'),
      'BJ' => t('Benin'),
      'BM' => t('Bermuda'),
      'BT' => t('Bhutan'),
      'BO' => t('Bolivia'),
      'BA' => t('Bosnia And Herzegovina'),
      'BW' => t('Botswana'),
      'BV' => t('Bouvet Island'),
      'BR' => t('Brazil'),
      'IO' => t('British Indian Ocean Territory'),
      'BN' => t('Brunei Darussalam'),
      'BG' => t('Bulgaria'),
      'BF' => t('Burkina Faso'),
      'BI' => t('Burundi'),
      'KH' => t('Cambodia'),
      'CM' => t('Cameroon'),
      'CA' => t('Canada'),
      'CV' => t('Cape Verde'),
      'KY' => t('Cayman Islands'),
      'CF' => t('Central African Republic'),
      'TD' => t('Chad'),
      'CL' => t('Chile'),
      'CN' => t('China'),
      'CX' => t('Christmas Island'),
      'CC' => t('Cocos (Keeling) Islands'),
      'CO' => t('Colombia'),
      'KM' => t('Comoros'),
      'CG' => t('Congo'),
      'CD' => t('Congo, Democratic Republic'),
      'CK' => t('Cook Islands'),
      'CR' => t('Costa Rica'),
      'CI' => t('Cote D\'Ivoire'),
      'HR' => t('Croatia'),
      'CU' => t('Cuba'),
      'CY' => t('Cyprus'),
      'CZ' => t('Czech Republic'),
      'DK' => t('Denmark'),
      'DJ' => t('Djibouti'),
      'DM' => t('Dominica'),
      'DO' => t('Dominican Republic'),
      'EC' => t('Ecuador'),
      'EG' => t('Egypt'),
      'SV' => t('El Salvador'),
      'GQ' => t('Equatorial Guinea'),
      'ER' => t('Eritrea'),
      'EE' => t('Estonia'),
      'ET' => t('Ethiopia'),
      'FK' => t('Falkland Islands (Malvinas)'),
      'FO' => t('Faroe Islands'),
      'FJ' => t('Fiji'),
      'FI' => t('Finland'),
      'FR' => t('France'),
      'GF' => t('French Guiana'),
      'PF' => t('French Polynesia'),
      'TF' => t('French Southern Territories'),
      'GA' => t('Gabon'),
      'GM' => t('Gambia'),
      'GE' => t('Georgia'),
      'DE' => t('Germany'),
      'GH' => t('Ghana'),
      'GI' => t('Gibraltar'),
      'GR' => t('Greece'),
      'GL' => t('Greenland'),
      'GD' => t('Grenada'),
      'GP' => t('Guadeloupe'),
      'GU' => t('Guam'),
      'GT' => t('Guatemala'),
      'GG' => t('Guernsey'),
      'GN' => t('Guinea'),
      'GW' => t('Guinea-Bissau'),
      'GY' => t('Guyana'),
      'HT' => t('Haiti'),
      'HM' => t('Heard Island & Mcdonald Islands'),
      'VA' => t('Holy See (Vatican City State)'),
      'HN' => t('Honduras'),
      'HK' => t('Hong Kong'),
      'HU' => t('Hungary'),
      'IS' => t('Iceland'),
      'IN' => t('India'),
      'ID' => t('Indonesia'),
      'IR' => t('Iran, Islamic Republic Of'),
      'IQ' => t('Iraq'),
      'IE' => t('Ireland'),
      'IM' => t('Isle Of Man'),
      'IL' => t('Israel'),
      'IT' => t('Italy'),
      'JM' => t('Jamaica'),
      'JP' => t('Japan'),
      'JE' => t('Jersey'),
      'JO' => t('Jordan'),
      'KZ' => t('Kazakhstan'),
      'KE' => t('Kenya'),
      'KI' => t('Kiribati'),
      'KR' => t('Korea'),
      'KW' => t('Kuwait'),
      'KG' => t('Kyrgyzstan'),
      'LA' => t('Lao People\'s Democratic Republic'),
      'LV' => t('Latvia'),
      'LB' => t('Lebanon'),
      'LS' => t('Lesotho'),
      'LR' => t('Liberia'),
      'LY' => t('Libyan Arab Jamahiriya'),
      'LI' => t('Liechtenstein'),
      'LT' => t('Lithuania'),
      'LU' => t('Luxembourg'),
      'MO' => t('Macao'),
      'MK' => t('Macedonia'),
      'MG' => t('Madagascar'),
      'MW' => t('Malawi'),
      'MY' => t('Malaysia'),
      'MV' => t('Maldives'),
      'ML' => t('Mali'),
      'MT' => t('Malta'),
      'MH' => t('Marshall Islands'),
      'MQ' => t('Martinique'),
      'MR' => t('Mauritania'),
      'MU' => t('Mauritius'),
      'YT' => t('Mayotte'),
      'MX' => t('Mexico'),
      'FM' => t('Micronesia, Federated States Of'),
      'MD' => t('Moldova'),
      'MC' => t('Monaco'),
      'MN' => t('Mongolia'),
      'ME' => t('Montenegro'),
      'MS' => t('Montserrat'),
      'MA' => t('Morocco'),
      'MZ' => t('Mozambique'),
      'MM' => t('Myanmar'),
      'NA' => t('Namibia'),
      'NR' => t('Nauru'),
      'NP' => t('Nepal'),
      'NL' => t('Netherlands'),
      'AN' => t('Netherlands Antilles'),
      'NC' => t('New Caledonia'),
      'NZ' => t('New Zealand'),
      'NI' => t('Nicaragua'),
      'NE' => t('Niger'),
      'NG' => t('Nigeria'),
      'NU' => t('Niue'),
      'NF' => t('Norfolk Island'),
      'MP' => t('Northern Mariana Islands'),
      'NO' => t('Norway'),
      'OM' => t('Oman'),
      'PK' => t('Pakistan'),
      'PW' => t('Palau'),
      'PS' => t('Palestinian Territory, Occupied'),
      'PA' => t('Panama'),
      'PG' => t('Papua New Guinea'),
      'PY' => t('Paraguay'),
      'PE' => t('Peru'),
      'PH' => t('Philippines'),
      'PN' => t('Pitcairn'),
      'PL' => t('Poland'),
      'PT' => t('Portugal'),
      'PR' => t('Puerto Rico'),
      'QA' => t('Qatar'),
      'RE' => t('Reunion'),
      'RO' => t('Romania'),
      'RU' => t('Russian Federation'),
      'RW' => t('Rwanda'),
      'BL' => t('Saint Barthelemy'),
      'SH' => t('Saint Helena'),
      'KN' => t('Saint Kitts And Nevis'),
      'LC' => t('Saint Lucia'),
      'MF' => t('Saint Martin'),
      'PM' => t('Saint Pierre And Miquelon'),
      'VC' => t('Saint Vincent And Grenadines'),
      'WS' => t('Samoa'),
      'SM' => t('San Marino'),
      'ST' => t('Sao Tome And Principe'),
      'SA' => t('Saudi Arabia'),
      'SN' => t('Senegal'),
      'RS' => t('Serbia'),
      'SC' => t('Seychelles'),
      'SL' => t('Sierra Leone'),
      'SG' => t('Singapore'),
      'SK' => t('Slovakia'),
      'SI' => t('Slovenia'),
      'SB' => t('Solomon Islands'),
      'SO' => t('Somalia'),
      'ZA' => t('South Africa'),
      'GS' => t('South Georgia And Sandwich Isl.'),
      'ES' => t('Spain'),
      'LK' => t('Sri Lanka'),
      'SD' => t('Sudan'),
      'SR' => t('Suriname'),
      'SJ' => t('Svalbard And Jan Mayen'),
      'SZ' => t('Swaziland'),
      'SE' => t('Sweden'),
      'CH' => t('Switzerland'),
      'SY' => t('Syrian Arab Republic'),
      'TW' => t('Taiwan'),
      'TJ' => t('Tajikistan'),
      'TZ' => t('Tanzania'),
      'TH' => t('Thailand'),
      'TL' => t('Timor-Leste'),
      'TG' => t('Togo'),
      'TK' => t('Tokelau'),
      'TO' => t('Tonga'),
      'TT' => t('Trinidad And Tobago'),
      'TN' => t('Tunisia'),
      'TR' => t('Turkey'),
      'TM' => t('Turkmenistan'),
      'TC' => t('Turks And Caicos Islands'),
      'TV' => t('Tuvalu'),
      'UG' => t('Uganda'),
      'UA' => t('Ukraine'),
      'AE' => t('United Arab Emirates'),
      'GB' => t('United Kingdom'),
      'US' => t('United States'),
      'UM' => t('United States Outlying Islands'),
      'UY' => t('Uruguay'),
      'UZ' => t('Uzbekistan'),
      'VU' => t('Vanuatu'),
      'VE' => t('Venezuela'),
      'VN' => t('Viet Nam'),
      'VG' => t('Virgin Islands, British'),
      'VI' => t('Virgin Islands, U.S.'),
      'WF' => t('Wallis And Futuna'),
      'EH' => t('Western Sahara'),
      'YE' => t('Yemen'),
      'ZM' => t('Zambia'),
      'ZW' => t('Zimbabwe'),
    );
    return $countries;
  }
}