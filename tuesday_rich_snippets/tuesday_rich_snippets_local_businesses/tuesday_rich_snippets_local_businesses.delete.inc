<?php

/**
 * Callback function from hook_menu().
 * Item : admin/config/tuesday/rich-snippets/local-businesses/delete/%
 */
function tuesday_rich_snippets_local_businesses_delete($form, &$form_state, $local_business_id = 0) {
  $local_business_delete = TuesdayRichSnippetsLocalBusinessApi::getLocalBusinessById($local_business_id);

  $form = array();

  $form['question'] = array(
    '#type' => 'markup',
    '#markup' => t('<p>Are you sure you want to delete Local Business: <em>@local_businesses_name</em> ? </p>', array('@local_businesses_name' => $local_business_delete['name'])),
  );

  $form['local_business_id'] = array(
    '#type' => 'hidden',
    '#value' => $local_business_id,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  $form['actions']['#submit'][] = 'tuesday_rich_snippets_local_businesses_delete_submit';
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), '/admin/config/tuesday/rich-snippets/local-businesses'),
  );

  return $form;
}

function tuesday_rich_snippets_local_businesses_delete_submit($form, &$form_state) {
  $local_business_id =  $form_state['values']['local_business_id'];

  TuesdayRichSnippetsLocalBusinessApi::delete($local_business_id);
  
  drupal_set_message(t('Local Business has been deleted.'), 'status');
  drupal_goto('/admin/config/tuesday/rich-snippets/local-businesses');
}