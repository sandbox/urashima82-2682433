<?php 

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function tuesday_rich_snippets_product_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'rich_snippet_product' => array(
      'label' => t('Rich Snippet Product'),
      'description' => t('Define Rich Snippets Product data.'),
      'default_widget' => 'field_rich_snippet_product_widget',
      'default_formatter' => 'field_rich_snippet_product_formatter',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function tuesday_rich_snippets_product_field_widget_info() {
  return array(
    'field_rich_snippet_product_widget' => array(
      'label' => t('Default Widget'),
      'field types' => array('rich_snippet_product'),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function tuesday_rich_snippets_product_field_formatter_info() {
  return array(
    'field_rich_snippet_product_formatter' => array(
      'label' => t('Application/Json in HEAD'),
      'field types' => array('rich_snippet_product'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function tuesday_rich_snippets_product_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $widget = $element;
  $widget['#delta'] = $delta;

  $values = isset($items[$delta]) ? $items[$delta] : array();
  $default_values = isset($items[$delta]['rich_snippet_product']) ? $items[$delta]['rich_snippet_product'] : $values;

  switch ($instance['widget']['type']) {

    case 'field_rich_snippet_product_widget':
      $widget += array(
        '#type' => 'fieldset',
      );
      $widget['name'] = array(
        '#type' => 'textfield',
        '#description' => t('The name of the product.'),
        '#title' => t('Name'),
        '#default_value' => isset($default_values['name']) ? $default_values['name'] : '',
      );
      $widget['mpn'] = array(
        '#type' => 'textfield',
        '#title' => t('MPN'),
        '#description' => t('Various identification properties : sku | gtin8 | gtin13 | gtin14 | mpn.'),
        '#default_value' => isset($default_values['mpn']) ? $default_values['mpn'] : '',
      );
      $widget['availability'] = array(
        '#type' => 'select',
        '#title' => t('Availability'),
        '#description' => t('Product availability.'),
        '#options' => array(
          'http://schema.org/InStock' => 'In stock',
          'http://schema.org/OutOfStock' => 'Out of stock',
        ),
        '#default_value' => isset($default_values['availability']) ? $default_values['availability'] : '',
      );
      $widget['price'] = array(
        '#type' => 'textfield',
        '#title' => t('Price'),
        '#description' => t('The price of the product.'),
        '#default_value' => isset($default_values['price']) ? $default_values['price'] : '',
      );
      $widget['price_currency'] = array(
        '#type' => 'textfield',
        '#description' => t('The currency used to describe the product price, in three-letter ISO 4217 format.'),
        '#title' => t('Currency'),
        '#default_value' => isset($default_values['price_currency']) ? $default_values['price_currency'] : '',
      );
      $default_price_valid_until = array();
      if(isset($default_values['price_valid_until']) && !empty($default_values['price_valid_until'])) {
        if(!is_array($default_values['price_valid_until'])) {
          $default_values['price_valid_until'] = explode('-', $default_values['price_valid_until']);
          $default_price_valid_until['year'] = $default_values['price_valid_until'][0];
          $default_price_valid_until['month'] = $default_values['price_valid_until'][1];
          $default_price_valid_until['day'] = $default_values['price_valid_until'][2];
        }
        else {
          $default_price_valid_until = $default_values['price_valid_until'];
        }
      }
      $widget['set_price_valid_until'] = array(
        '#type' => 'checkbox',
        '#description' => t('Define a date after which the price will no longer be available.'),
        '#title' => t('Set Price valid until'),
        '#default_value' => !empty($default_values['price_valid_until']),
      );
      $widget['price_valid_until'] = array(
        '#type' => 'date',
        '#title' => t('Price valid until'),
        '#date_format' => 'Y-m-d',
        '#default_value' => !empty($default_price_valid_until) ? $default_price_valid_until : '',
        '#states' => array(
          'visible' => array(
            ':input[name="' . $field['field_name'] .'[' . $langcode . ']['. $delta .'][rich_snippet_product][set_price_valid_until]"]' => array('checked' => TRUE)),
          ),
      );
      $widget['item_condition'] = array(
        '#type' => 'select',
        '#title' => t('Item condition'),
        '#description' => t('A predefined value of the condition of the product or service.'),
        '#options' => array(
          'http://schema.org/NewCondition' => 'New',
          'http://schema.org/UsedCondition' => 'Used',
          'http://schema.org/RefurbishedCondition' => 'Refurbished',
          'http://schema.org/DamagedCondition' => 'Damaged',
        ),
        '#default_value' => isset($default_values['item_condition']) ? $default_values['item_condition'] : '',
      );
      $widget['brand'] = array(
        '#type' => 'textfield',
        '#title' => t('Brand'),
        '#description' => t('The brand of the product.'),
        '#default_value' => isset($default_values['brand']) ? $default_values['brand'] : '',
      );
       $widget['seller'] = array(
        '#type' => 'textfield',
        '#description' => t('An entity which offers the services / goods. A seller may also be a provider.'),
        '#title' => t('Seller'),
        '#default_value' => isset($default_values['seller']) ? $default_values['seller'] : '',
      );
      $widget['image'] = array(
        '#type' => 'textfield',
        '#description' => t('The URL of a product photo.'),
        '#title' => t('Image'),
        '#default_value' => isset($default_values['image']) ? $default_values['image'] : '',
      );
      $widget['description'] = array(
        '#type' => 'textarea',
        '#title' => t('Description'),
        '#description' => t('Product description.'),
        '#default_value' => isset($default_values['description']) ? $default_values['description'] : '',
      );

      $widget['tokens'] = array(
        '#theme' => 'token_tree_link',
        '#token_types' => array('node'),
        '#weight' => 99,
      );

      break;
  }

  $element['rich_snippet_product'] = $widget;
  return $element;
}

/**
 * Implements hook_field_presave().
 */
function tuesday_rich_snippets_product_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    if (!empty($item['rich_snippet_product']['set_price_valid_until'])) {
      $price_valid_until = $item['rich_snippet_product']['price_valid_until'];
      $price_valid_until = $price_valid_until['year'] .  '-' . $price_valid_until['month'] . '-' . $price_valid_until['day'];
      $item['rich_snippet_product']['price_valid_until'] = $price_valid_until;  
    }
    else {
      $item['rich_snippet_product']['price_valid_until'] = '';
    }

    $items[$delta] = $item['rich_snippet_product'];
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function tuesday_rich_snippets_product_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'field_rich_snippet_product_formatter' :
      foreach ($items as $delta => $item) {

        $json_output = tuesday_rich_snippets_product_json_prepare($item, $entity_type, $entity);
        $script = array(
          '#type' => 'markup',
          '#markup' => '<script type="application/ld+json">' . $json_output . '</script>' . "\n",
        );
        drupal_add_html_head($script, 'tuesday_rich_snippets_product');
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_field_validate().
 */
function tuesday_rich_snippets_product_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['rich_snippet_product'])) {
      // Do some validation.
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function tuesday_rich_snippets_product_field_is_empty($item, $field) {
  return empty($item['rich_snippet_product']);
}

/**
 * Implements hook_form_field_ui_field_edit_form_alter().
 */
function tuesday_rich_snippets_product_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#field']['type'] == 'rich_snippet_product') {
    $form['field']['cardinality']['#options'] = array(1 => 1);
  }
}

/**
 * This function prepare the output in JSON of field item.
 */
function tuesday_rich_snippets_product_json_prepare($item, $entity_type, $entity) {
  foreach ($item as $key => $field) {
    $item[$key] = token_replace($field, array($entity_type => $entity));
  }

  $description = trim(preg_replace('/\s+/', ' ', drupal_html_to_text($item['description'])));

  $output = array(
    '@context' => 'http://schema.org/',
    '@type' => 'Product',
    'name' => $item['name'],
    'image' => $item['image'],
    'description' => $description,
    'mpn' => $item['mpn'],
    'brand' => array(
      '@type' => 'Thing',
      'name' => $item['brand'],
    ),
    'offers' => array(
      '@type' => 'Offer',
      'priceCurrency' => $item['price_currency'],
      'price' => $item['price'],
      'priceValidUntil' => $item['price_valid_until'],
      'itemCondition' => $item['item_condition'],
      'availability' => $item['availability'],
      'seller' => array(
        '@type' => 'Organization',
        'name' => $item['seller'],
      )
    )
  );

  if (!defined('JSON_PRETTY_PRINT')) {
    define('JSON_PRETTY_PRINT', 128);
  }
  if (!defined('JSON_UNESCAPED_UNICODE')) {
    define('JSON_UNESCAPED_UNICODE', 256);
  }

  $json_output = stripslashes(json_encode($output, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));;

  return $json_output;
}