<?php

/**
 * Implements hook_menu().
 */
function tuesday_geocomplete_menu() {

  $items = array();

  $items['admin/config/tuesday/geocomplete'] = array(
    'title' => t('Geocomplete'),
    'description' => t('Geocomplete configuration.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tuesday_geocomplete_admin'),
    'access arguments' => array('administer tuesday modules'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

function tuesday_geocomplete_admin($form, &$form_state) {
  $form = array();

  $form['tuesday_geocomplete_api'] = array(
    '#type' => 'textfield',
    '#title'=> t('Enter the Google API key'),
    '#description' => t('Obtain a free Google Geocoding API Key at https://developers.google.com/maps/documentation/geocoding/#api_key'),
    '#default_value' => variable_get('tuesday_geocomplete_api',''),
  );

  $form['tuesday_geocomplete_selector'] = array(
    '#type' => 'textarea',
    '#title'=> t('Selector'),
    '#description' => t('Enter CSS selector for each component to apply geocomplete. <em>One per line</em>.'),
    '#default_value' => variable_get('tuesday_geocomplete_selector',''),
  );

  $form['tuesday_geocomplete_countries'] = array(
    '#type' => 'textarea',
    '#title'=> t('Countries'),
    '#description' => t('Enter list of ISO country code. <em>One per line</em>.'),
    '#default_value' => variable_get('tuesday_geocomplete_countries',''),
  );

  return system_settings_form($form);
}

function tuesday_geocomplete_init() {
  $api_key = variable_get('tuesday_geocomplete_api','');
  $selector = variable_get('tuesday_geocomplete_selector','');
  $countries = variable_get('tuesday_geocomplete_countries','');

  if(!empty($api_key) && !empty($selector)) {
    drupal_add_js('https://maps.googleapis.com/maps/api/js?key=' . $api_key . '&libraries=places', 'external');
    libraries_load('geocomplete');


    $selector = explode("\n", $selector);
    $selector = implode(', ', $selector);

    $setting = array(
      'tuesday_geocomplete' => array(
        'selector' => $selector,
      ),
    );
    
    if(!empty($countries)) {
      $countries = explode("\n", $countries);
      foreach ($countries as $key => $country) {
        if(empty($country)) {
          unset($countries[$key]);
        }
        else {
          $countries[$key] = strtolower(trim($country));
        }
      }
      $setting['tuesday_geocomplete']['countries'] = $countries;
    }

    drupal_add_js($setting, 'setting');
    drupal_add_js(drupal_get_path('module', 'tuesday_geocomplete') .'/tuesday_geocomplete.js');
  }
}

function tuesday_geocomplete_libraries_info() {
  $libraries['geocomplete'] = array(
    'name' => 'Geocomplete library',
    'vendor url' => 'https://github.com/ubilabs/geocomplete',
    'download url' => 'https://github.com/ubilabs/geocomplete/archive/master.zip',
    'version callback' => 'tuesday_geocomplete_version_callback',
    'files' => array(
      'js' => array('jquery.geocomplete.min.js'),
    ),
  );

  return $libraries;
}

function tuesday_geocomplete_version_callback() {
  return TRUE;
}