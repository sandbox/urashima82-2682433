(function ($, Drupal) {

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.tuesday_geocomplete = {
    attach: function (context, settings) {

      var setting = settings.tuesday_geocomplete;
      var options = {};
      
      if(setting.hasOwnProperty('countries')) {
        options.componentRestrictions = {
          country: setting.countries,
        };
      }

      $(setting.selector).geocomplete(options);

    }
  };

})(jQuery, Drupal);