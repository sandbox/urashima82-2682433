<?php

class tuesdayUtils {

  protected static $instance = NULL;
  
  protected function __construct() { }
	
	/**
	 *	Create instance.
	 *	Call tuesday_utils_api_get_instance() to create instance (see .module)
	 */
  public static function getInstance() {
    if (!isset(static::$instance)) {
      static::$instance = new static();
    }
    return static::$instance;
  }
	
	/**
	 *	Return a string with all available functions
	 */
	public function help($show_dsm = false) {
  	$this->function_list = array(
  	'structureWebformSubmittedData' => '
  	Return a structured array of submitted datas
  	Args: 
  		- sid: submission SID
  	',
  	'getReferencingNodeFromCommerceProduct' => '
  	Return a node nid
  	Args: 
  		- commerce_product: a loaded commerce_product
  		- content_type: type of nodes to look for
  		- field_name: machine name of field the to look for
  	',
  	'tuesbug' => '
  	Debugging function
  	Args: 
  		- thing_to_debug: structure to debug
  		- label: the message label
  		- mode: object_log, print_r or dsm (default)
  	'
	  );
	  if($show_dsm) {
	  	if(function_exists('dsm')) {
	  		dsm($this->function_list,'in class utils, function help()');
	  	}
	  }
	  else {
			return $this->function_list;
		}
	}

	/**
	 *  	Return a structured array of submitted datas
   *		Args: 
   *		- sid: submission SID
	 */
	public function structureWebformSubmittedData($sid) {
		module_load_include('inc','webform','includes/webform.submissions');
		$structured_datas = array();
		$submissions = $submission = false;
		if(is_object($sid)){
			$submission = $sid;
		}
		elseif((int)$sid > 0) {
			if($submissions = webform_get_submissions(array('sid' => $sid))) {
				$submission = $submissions[$sid];
			}
		}
		if($submission) {
			$webform = node_load($submission->nid);
			$components = $webform->webform['components'];
			$submission_data = $submission->data;
			foreach($components as $key => $component) {
				if(isset($submission_data[$key])) {
					$structured_datas[$component['form_key']] = array_shift($submission_data[$key]);
				}
			}
		}
		return $structured_datas;
	}

	/**
	 *   	Return a node nid
	 *  	Args: 
 	 *		- commerce_product: a loaded commerce_product
	 * 		- content_type: type of nodes to look for
   *		- field_name: machine name of field the to look for
   *		- return_first_only (optional)
	 */
	public function getReferencingNodeFromCommerceProduct($commerce_product, $content_type, $field_name, $return_first_only = true) {
		// Itterate through all fields which refer to commerce products.
		$nids = array();
		$pid = $commerce_product->product_id; //->value();
		$query = new EntityFieldQuery;
		$query->entityCondition('entity_type', 'node', '=')
			->propertyCondition('type', $content_type)
			->fieldCondition($field_name, 'product_id', array($pid), 'IN')
			->range(0, 10);

		if ($result = $query->execute()) {
			// Return node id.
			if(isset($result['node'])) {
				foreach($result['node'] as $nid => $node_object) {
					$nids = array_merge($nids, array($nid));
				}
			}
		}
		return (count($nids) > 0) ? ($return_first_only ? array_pop($nids) : $nids) : false;
	}

	/**
   *	Debugging function
   *	Args: 
   *		- thing_to_debug: structure to debug
   *		- label: the message label
   *		- mode: object_log, print_r or dsm (default)
	 */
	public function tuesbug($thing_to_debug, $label, $mode = 'dsm') {
		switch($mode) {
			case 'object_log':
				if(function_exists('object_log')) {
					object_log($label, $thing_to_debug);
				}
				else {
					drupal_set_message('Object_log module not present', 'error');
				}
			break;
			case 'print_r':
				echo '<pre>'. print_r($thing_to_debug, true) .'</pre>';
			break;
			default:
				// default to dsm
				if(function_exists('dsm')) {
					dsm($thing_to_debug, $label);
				}
				else {
					drupal_set_message('Devel module not present', 'error');
				}
			break;
		}
	}
	
}