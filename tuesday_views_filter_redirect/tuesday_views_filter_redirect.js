(function (Drupal, $){
  Drupal.behaviors.tuesday_views_filter_redirect = {
    attach: function (context, settings) {
      setting = settings.tuesday_views_filter_redirect;
      if(!setting.is_default_redirect_path){
        var filters = setting.real_filters;
        var emptyChoose = true;

        $.each(filters, function(index, value) {
          $('input[name="' + filters[index] + '[]"]').each(function(){
            if($(this).attr('type') == 'checkbox' && $(this).prop('checked')) {
              emptyChoose = false;
            }
          })
        });

        if(emptyChoose) {
          console.log(setting.default_redirect_path);
          window.location.href = setting.default_redirect_path;
        }
      }

      $('.views-exposed-form .form-type-bef-checkbox').each(function(){
        if($(this).find('input[type="checkbox"]').prop('checked') == true){
          $(this).addClass('highlight');
        }
        else {
          $(this).removeClass('highlight');
        }
      });
    }
  };
})(Drupal, jQuery);