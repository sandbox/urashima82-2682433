<?php

/**
 * Callback function from hook_menu().
 * Item : admin/config/tuesday/views-filter-redirect
 */
function tuesday_views_filter_redirect_admin($form, &$form_state) {
  $form = array();

  $form['action']['add_view_filrer_redirect'] = array(
    '#type' => 'markup',
    '#markup' => '',
    '#prefix' => '<ul class="action-links">',
    '#suffix' => '</ul>',

    'add_view_filrer_redirect' => array(
      '#type' => 'markup',
      '#markup' => l(t('Add a View Filter Redirect'),'/admin/config/tuesday/views-filter-redirect/add'),
      '#prefix' => '<li>',
      '#suffix' => '</li>',
    )
  );

  $header = array(
    array('data' => t('#Id'), 'field' => 't.id'),
    array('data' => t('View name'), 'field' => 't.view_name'),
    array('data' => t('View display'), 'field' => 't.view_display'),
    array('data' => t('Is active'), 'field' => 't.is_active'),
    array('data' => t('Action')),
  );

  $result = db_select('tuesday_views_filter_redirect', 't')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(50)
    ->fields('t', array('id', 'view_name', 'view_display', 'is_active'))
    ->orderByHeader($header)
    ->execute();

  $rows = array();

  if($result->rowCount()) {
    foreach ($result as $row) {
      $final_row = (array) $row;
      $final_row['action'] = ' <a href="/admin/config/tuesday/views-filter-redirect/edit/' . $final_row['id'] . '">' . t('Edit') . '</a> ';
      $final_row['action'] .= ' <a href="/admin/config/tuesday/views-filter-redirect/delete/' . $final_row['id'] . '">' . t('Delete') . '</a> ';
      
      $final_row['is_active'] = $final_row['is_active'] ? t('Yes') : t('No');

      $rows[] = array('data' => $final_row);
    }
  }
  else {
    $rows[] = array(
      'default_button' => array(
        'data' => '<a href="/admin/config/tuesday/views-filter-redirect/add" class="button">' . t('Add a View Filter Redirect') . '</a>',
        'colspan' => 5,
      ),
    );
  }

  // Build the table for the nice output.
  $build['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  $build['tablesort_pager'] = array(
    '#theme' => 'pager',
  );

  $form['tablesort_table'] = array(
    '#type' => 'markup',
    'markup' => $build['tablesort_table'],
  );

  $form['tablesort_pager'] = array('#markup' => theme('pager'));

  return $form;
}