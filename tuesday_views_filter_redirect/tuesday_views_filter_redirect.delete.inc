<?php

/**
 * Callback function from hook_menu().
 * Item : admin/config/tuesday/rich-snippets/local-businesses/delete/%
 */
function tuesday_views_filter_redirect_delete($form, &$form_state, $id = 0) {

  $form = array();

  $form['question'] = array(
    '#type' => 'markup',
    '#markup' => t('<p>Are you sure you want to delete Views Filter Redirect: <em>@id</em> ? </p>', array('@id' => $id)),
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  $form['actions']['#submit'][] = 'tuesday_views_filter_redirect_delete_submit';
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), '/admin/config/tuesday/views-filter-redirect'),
  );

  return $form;
}

function tuesday_views_filter_redirect_delete_submit($form, &$form_state) {
  $id =  $form_state['values']['id'];

  db_delete('tuesday_views_filter_redirect')
    ->condition('id', $id)
    ->execute();
  
  drupal_set_message(t('Views Filter Redirect has been deleted.'), 'status');
  drupal_goto('/admin/config/tuesday/views-filter-redirect');
}