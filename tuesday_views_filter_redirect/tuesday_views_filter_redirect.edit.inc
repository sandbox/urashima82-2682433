<?php

/**
 * Callback function from hook_menu().
 * Item : admin/config/tuesday/views-filter-redirect
 */
function tuesday_views_filter_redirect_edit($form, &$form_state, $id = 0) {
  $form = array();
  $record = array();
  $default_is_active = TRUE;
  $default_view = '';
  $default_view_display = '';
  $default_default_redirect_path = '';
  $filters_redirect_data = array();
  $filters_delta = 0;
  $filter_delta_to_delete = -1;
  
  if(!empty($id)) {
    $record = tuesday_views_filter_redirect_get_record($id);
    $default_view = $record['view_name'];
    $default_view_display = $record['view_display'];
    $default_is_active = $record['is_active'];
    $default_default_redirect_path = $record['default_redirect_path'];
    $filters_redirect_data = $record['exposed_filters_redirect_data'];
  }

  $default_view = !empty($form_state['values']['view_name']) ? $form_state['values']['view_name'] : $default_view;
  $default_view_display = !empty($form_state['values']['view_display']) ? $form_state['values']['view_display'] : $default_view_display;

  if(!empty($record)){
    $filters_delta = count($record['exposed_filters_redirect_data']);
  }
  if(empty($form_state['storage']['filters_delta'])) {
    $form_state['storage']['filters_delta'] = $filters_delta;
  }
  else {
    $filters_delta = $form_state['storage']['filters_delta'];
  }
  
  // Popupulate list of views.
  $views_list = views_get_all_views();
  $views_options = array();
  foreach ($views_list as $view_name => $view_info) {
    if(empty($view_info->disabled) || !$view_info->disabled) {
      $views_options[$view_name] = $view_info->human_name . ' - (ID: ' . $view_name . ')';
    }
  }

  // Populate list of display.
  $view_display_options = array();
  if(!empty($default_view)) {
    $default_view = views_get_view($default_view);
    foreach ($default_view->display as $display_id => $display_info) {
      $view_display_options[$display_id] = $display_info->display_title . ' - (ID: ' . $display_id . ')';
    }
  }

  // Popupulate list of exposed filters
  $view_exposed_filters_options = array();
  if(!empty($default_view_display)) {
    $default_view_display = $default_view->display[$default_view_display];
    if($default_view_display->id != 'default' && isset($default_view->display['default']->display_options['filters'])) {
      foreach ($default_view->display['default']->display_options['filters'] as $filter_id => $filter_info) {
        if(isset($filter_info['exposed']) && $filter_info['exposed']) {
          $view_exposed_filters_options[$filter_id] = $filter_info['expose']['label'] . ' - (ID: ' . $filter_id . ')';
        }
      }
    }
    
    if(isset($default_view_display->display_options['filters'])) {
      foreach ($default_view_display->display_options['filters'] as $filter_id => $filter_info) {
        if(isset($filter_info['exposed']) && $filter_info['exposed']) {
          $view_exposed_filters_options[$filter_id] = $filter_info['expose']['label'] . ' - (ID: ' . $filter_id . ')';
        }
      }
    }
  }

  if(isset($form_state['triggering_element']['#name'])) {
    $triggering_element_name = $form_state['triggering_element']['#name'];
    if($triggering_element_name == 'add-filter') {
      $filters_delta++;
      $form_state['storage']['filters_delta']++;
    }
    if(strpos($triggering_element_name, 'delete-filter-') !== FALSE) {
      $filter_delta_to_delete = str_replace('delete-filter-', '', $triggering_element_name);
      $form_state['storage']['filters_delta']--;
    }
  }

  if(isset($form_state['triggering_element']['#name']) && $form_state['triggering_element']['#name'] != 'delete-filter-') {
    $element_to_delete = $form_state['triggering_element']['#name'];
    $index_to_delete = str_replace('delete-opening-hour-', '', $element_to_delete);
  }
  

  $form['is_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is active'),
    '#default_value' => $default_is_active,
  );

  $form['view_name'] = array(
    '#type' => 'select',
    '#title' => t('Select a view'),
    '#options' => $views_options,
    '#default_value' => !empty($default_view) ? $default_view->name : '',
    '#required' => TRUE,
    '#empty_option' => t('- Select a view -'),
    '#ajax' => array(
      'callback' => 'tuesday_views_filter_redirect_edit_view_display_callback',
      'wrapper' => 'view-display',
    ),
  );

  $form['view_display'] = array(
    '#type' => 'select',
    '#title' => t('Select a display'),
    '#required' => TRUE,
    '#options' => $view_display_options,
    '#default_value' => !empty($default_view_display) ? $default_view_display->id : '',
    '#empty_option' => t('- Select a view display -'),
    '#prefix' => '<div id="view-display">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'callback' => 'tuesday_views_filter_redirect_edit_view_exposed_filters_callback',
      'wrapper' => 'view-exposed-filters',
    ),
  );

  $form['default_redirect_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Default redirect path'),
    '#default_value' => $default_default_redirect_path,
  );

  $form['view_exposed_filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exposed filters'),
    '#prefix' => '<div id="view-exposed-filters">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  $form['view_exposed_filters']['filters'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="view-exposed-filters-item">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  for ($i = 0 ; $i < $filters_delta; $i++) {

    if($i != $filter_delta_to_delete) {
      $form['view_exposed_filters']['filters'][$i] = array(
        '#type' => 'fieldset',
        '#title' => t('Item'),
        '#id' => 'exposed-filter-item-' . $i,
        '#tree' => TRUE,
      );

      $form['view_exposed_filters']['filters'][$i]['exposed_filter'] = array(
        '#type' => 'select',
        '#title' => t('Exposed filter'),
        '#options' => $view_exposed_filters_options,
        '#default_value' => !empty($filters_redirect_data) ? $filters_redirect_data[$i]['exposed_filter'] : '',
      );

      $form['view_exposed_filters']['filters'][$i]['exposed_filter_value'] = array(
        '#type' => 'textfield',
        '#title' => t('Exposed filter value'),
        '#default_value' => !empty($filters_redirect_data) ? $filters_redirect_data[$i]['exposed_filter_value'] : '',
      );

      $form['view_exposed_filters']['filters'][$i]['redirect'] = array(
        '#type' => 'textfield',
        '#title' => t('Redirect path'),
        '#default_value' => !empty($filters_redirect_data) ? $filters_redirect_data[$i]['redirect'] : '',
      );

      $form['view_exposed_filters']['filters'][$i]['delete_filter_' . $i] = array(
        '#type' => 'button',
        '#value' => t('Delete'),
        '#href' => '',
        '#name' => 'delete-filter-' . $i,
        '#ajax' => array(
          'callback' => 'tuesday_views_filter_redirect_edit_delete_filter_callback',
          'wrapper' => 'view-exposed-filters-item',
        ),
      );
    }
  }

  $form['view_exposed_filters']['add_filter'] = array(
    '#type' => 'button',
    '#value' => t('Add'),
    '#href' => '',
    '#name' => 'add-filter',
    '#ajax' => array(
      'callback' => 'tuesday_views_filter_redirect_edit_add_filter_callback',
      'wrapper' => 'view-exposed-filters-item',
    ),
  );  

  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
    '#weight' => 99,
  );

  if(!empty($id)) {
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $id,
    );
  }

  $form['#validate'][] = 'tuesday_views_filter_redirect_edit_validate';
  $form['#submit'][] = 'tuesday_views_filter_redirect_edit_submit';

  return $form;
  
}

function tuesday_views_filter_redirect_edit_validate($form, $form_state) {
  //TODO
}

function tuesday_views_filter_redirect_edit_submit($form, $form_state) {
  $record = tuesday_views_filter_redirect_prepare_record($form_state);
  tuesday_views_filter_redirect_save_record($record);
  drupal_set_message(t('Views Filter Redirect has been saved.'));
  drupal_goto('/admin/config/tuesday/views-filter-redirect');
}

function tuesday_views_filter_redirect_edit_view_display_callback($form, $form_state) {
  return $form['view_display'];
}

function tuesday_views_filter_redirect_edit_view_exposed_filters_callback($form, $form_state) {
  return $form['view_exposed_filters'];
}

function tuesday_views_filter_redirect_edit_add_filter_callback($form, $form_state) {
  return $form['view_exposed_filters']['filters'];
}

function tuesday_views_filter_redirect_edit_delete_filter_callback($form, $form_state) {
  return $form['view_exposed_filters']['filters'];
}

function tuesday_views_filter_redirect_prepare_record($form_state){
  $values = $form_state['values'];
  $record = array(
    'is_active' => $values['is_active'],
    'view_name' => $values['view_name'],
    'view_display' => $values['view_display'],
    'default_redirect_path' => $values['default_redirect_path'],
    'exposed_filters_redirect_data' => array(),
  );
  if(isset($values['id'])) {
    $record['id'] = $values['id'];
  }
  foreach ($values['view_exposed_filters']['filters'] as $key => $config) {
    $record['exposed_filters_redirect_data'][] = array(
      'exposed_filter' => $config['exposed_filter'],
      'exposed_filter_value' => $config['exposed_filter_value'],
      'redirect' => $config['redirect'],
    );
  }
  return $record;
}